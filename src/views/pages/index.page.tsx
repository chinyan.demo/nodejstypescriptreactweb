import {useContext,FC} from "react";
import { RepositoryContext } from "../../repositories/repositoryContext";
import {useGet} from "../../helpers/hooks/xhr"
import ENUM_URL from "../../helpers/constants/url";


const IndexPage:FC = () => {
    const { errorRepository } = useContext(RepositoryContext)!;
    const {error:errorState} = errorRepository.errorRepo
    const { data, error} = useGet<{code:number,msg:string}>(ENUM_URL.HEALTH_CHECK)
    if(error) console.table(error)
    const popup:object = {...data, ...(errorState.code!=200)&&errorState }
    return(
        <>
            <h1>Index Page</h1>
            {JSON.stringify(popup,undefined,2)}
        </>


    )
}

export default IndexPage;