import { FC, useState } from "react";
import ENUM_URL from "../../helpers/constants/url";
import { InputComponent, SubmitComponent } from "../components/form";
import { LandingLayout } from "../components/layout/landing.layout";
import {
  nameValidate,
  emailValidate,
  passwordValidate,
  registerValidate,
} from "../../validators/auth.validate";
import { useNavigate } from "react-router-dom";

const RegisterForm: FC = () => {
  const [formData, setFormData] = useState<{ [key: string]: any }>({});
  const navigate = useNavigate();

  const onSuccess = () => {
    alert("Registered successfully");
    navigate("/");
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <form>
          <InputComponent
            label="name"
            type="text"
            fieldname="name"
            state={formData}
            setState={setFormData}
            joi={nameValidate}
          />
          <InputComponent
            label="email"
            type="email"
            fieldname="email"
            state={formData}
            setState={setFormData}
            joi={emailValidate}
          />
          <InputComponent
            label="password"
            type="password"
            fieldname="password"
            state={formData}
            setState={setFormData}
            joi={passwordValidate}
          />
          <InputComponent
            label="confirm password"
            type="password"
            fieldname="confirmPassword"
            state={formData}
            setState={setFormData}
            joi={passwordValidate}
          />
          <SubmitComponent
            label="Register"
            resHandler={onSuccess}
            url={ENUM_URL.AUTH.REGISTER}
            payload={formData}
            joi={registerValidate}
            className="btn btn-primary"
            iconClass="fa fa-user-plus"
          />
        </form>
      </div>
    </div>
  );
};
const RegisterPage: FC = () => {
  return (
    <LandingLayout title="Register Page">
      <RegisterForm />
    </LandingLayout>
  );
};

export default RegisterPage;
