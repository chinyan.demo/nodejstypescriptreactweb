import { FC, useContext } from "react";
import whiteBgImg from "/assets/chat/bgImg-white.jpg";
import blackBgImg from "/assets/chat/bgImg-black.png";
import "./message.css";
import {
  MessageSpace,
  UserInfo,
} from "../../../components/chat";
import { MessagesInput } from "../../../components/chat/MessageInput";
import { FriendListing } from "../../../components/chat/FriendList";

import { useMessageControllers } from "../../../../controllers/message.controller";
import { useOnlineUserFetch } from "../../../../repositories/services/message.services";
import { RepositoryContext } from "../../../../repositories/repositoryContext";

const MessagesPage: FC = () => {
  const { authRepository,messageRepository } = useContext(RepositoryContext)!;
  const { auth } = authRepository.authStates;
  const { allConversation } = messageRepository.chatListStates;
  const { conversationID } = messageRepository.selectedChatStates;
  const { selectChat } = useMessageControllers();
  useOnlineUserFetch();

  return (
    <div className="h-100">
      <div className="row d-flex">
        <div className="col col-xl-8 col-md-8 col-sm-12 col-xs-12 p-0">
          <div
            className="p-3 vh-100"
            style={{
              backgroundImage: `url(${whiteBgImg})`,
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              height: "100vh",
              overflow: "auto",
            }}
          >
            <MessageSpace
              chats={allConversation[conversationID]?.chats}
              users={allConversation[conversationID]?.users}
              type={allConversation[conversationID]?.type}
              name={allConversation[conversationID]?.name}
            />
          </div>
        </div>
        <div
          className="col col-lg-4 col-md-4 col-sm-12 col-xs-12 vh-100"
          style={{
            backgroundImage: `url(${blackBgImg})`,
          }}
        >
          <UserInfo name={auth.profile?.name as string} />
          <FriendListing selectChat={selectChat} />
        </div>
      </div>
      <MessagesInput />
    </div>
  );
};

export default MessagesPage;
