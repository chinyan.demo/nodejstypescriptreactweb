import { FC } from "react";
import { Breadcrumb } from "../../../components/navbar/breadcrumb";
import { Accordion } from "../../../components/bootstrap/Accordion";
import { CardSliderCol3 } from "../../../components/bootstrap/Cards";
import public_event from "/assets/public_event.jpg"
import "./conference.css"

const ConferencePage:FC = () => {
    const breadcrumb = [{url:"/private/home", label:"Conference", active:true, labelIcon:"fas fa-video"}]
    const publicConference = [{
        imageSrc:public_event,
        title:"Public Event",
        description:"Test WebRTC, Websocket, and Peer to Peer stack ."
    }]
    return (
        <>
            <Breadcrumb props={breadcrumb}/>
            <Accordion title="Public Conference" description="This is public conferences hosted by sponsor groups">
                <CardSliderCol3 data={publicConference}/>
            </Accordion>
        </>
    )
}

export default ConferencePage