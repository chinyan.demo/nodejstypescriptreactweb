/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {FC} from "react";
import ENUM_URL from "../../helpers/constants/url";
import {InputComponent,SubmitComponent} from "../components/form"
import {LandingLayout} from "../components/layout/landing.layout"
import {
    emailValidate, passwordValidate, loginValidate
} from "../../validators/auth.validate"
import { useLoginController } from "../../controllers/auth.controller";

const LoginForm:FC = () => {
        const {formData,setFormData,loginHandler} = useLoginController()

    return (
        <form>
            <InputComponent label="email" type="email" fieldname="email" state={formData} setState={setFormData}  joi={emailValidate}/>
            <InputComponent label="password" type="password" fieldname="password" state={formData} setState={setFormData}  joi={passwordValidate}/>
            <SubmitComponent 
                label="Login" 
                url={ENUM_URL.AUTH.LOGIN} 
                payload={formData} 
                joi={loginValidate} 
                className="btn btn-success" 
                iconClass="fa fa-power-off"
                resHandler={loginHandler}
            />
        </form>
    )
}


const LoginPage:FC = () => {
    return(
        <LandingLayout title="Login Page">
            <LoginForm/>
        </LandingLayout>
    )
}

export default LoginPage