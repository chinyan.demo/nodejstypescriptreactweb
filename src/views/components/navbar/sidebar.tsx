import { FC, JSX } from "react";
import { Link } from "react-router-dom";

export const HomeSideNav:FC<{data:Array<{icon:string,label:string,url:string,active:boolean}>,children:JSX.Element}> = ({data,children}) => {
    return (
        <div className="container-fluid">
            <div className="row flex-nowrap">
                <div className="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                    <div className="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                        <Link to="/" className="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                            <span className="fs-5 d-none d-sm-inline">Menu</span>
                        </Link>
                        <ul className="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start mx-auto" id="menu">
                            {data.map(doc=>{
                                return (
                                    <li className="nav-item mx-auto" key={doc.label}>
                                        <Link to={doc.url} className={`nav-link align-middle mx-auto ${(doc.active==true)?" active ":""}`}>
                                            <b>
                                                <span className="ms-1 d-none d-sm-inline"><i className={doc.icon}></i>&nbsp;&nbsp;&nbsp;&nbsp;{doc.label}</span>
                                            </b>
                                        </Link>
                                    </li>
                                )
                            })}
                        </ul>
                    </div>
                </div>
                <div className="col">
                    {children}
                </div>
            </div>
        </div>
    )
}