import { FC } from "react";
import { Link } from "react-router-dom";

export const Breadcrumb:FC<{props:Array<{url:string,label:string,labelIcon?:string,active:boolean}>}> = ({props}) => {
    return (
        <div style={{paddingTop:"1rem"}}>
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    {
                        props.map((prop,idx)=>{
                            const {url,label,active,labelIcon} = prop
                            return(
                                (active)?(
                                    <li className="breadcrumb-item active"  aria-current="page" key={idx}>
                                        <span className="h4"> <i className={labelIcon}></i> &nbsp;{label}</span>
                                    </li> 
                                ):(
                                    <li className="breadcrumb-item" key={idx}>
                                        <Link to={url}><span className="h4">{label}</span></Link>
                                    </li> 
                                )
                            )
                        })
                    }
                </ol>
            </nav>
            <hr/>
        </div>
    )
}