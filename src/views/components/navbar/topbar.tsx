import { FC, useContext } from "react";
import { Link } from "react-router-dom";
import { SocketContext } from "../../../helpers/websocket/socketProvider";
import { WorkerContext } from "../../../worker/workerContext";

export const HomeTopNav: FC = () => {
  const { socket } = useContext(SocketContext);
  const {backgroundWorker} = useContext(WorkerContext)
  return (
    <div className="navbar navbar-dark bg-dark">
      <div className="d-flex justify-content-start">
        <Link className="navbar-brand " to="/private/home">
          &nbsp;&nbsp;<i className="fa fa-home"></i> &nbsp;Home
        </Link>
      </div>

      <div className="d-flex justify-content-end">
        <Link className="navbar-brand" to="#">
          <i className="fa fa-user"></i> &nbsp;
        </Link>
        <Link className="navbar-brand" to="#">
          <i className="fa fa-bell"></i> &nbsp;
        </Link>
        <Link
          className="navbar-brand"
          onClick={() => {
            socket?.disconnect();
            backgroundWorker?.terminate()
            Object.keys(sessionStorage).forEach(key=>{sessionStorage.removeItem(key);})
            Object.keys(localStorage).forEach(key=>{localStorage.removeItem(key);})
          }}
          to="/"
        >
          <i className="fa fa-power-off text-danger"></i>&nbsp;{" "}
        </Link>
      </div>
    </div>
  );
};
