import { FC,JSX, useCallback, Dispatch } from "react";
import "./video.css"
import { Link } from "react-router-dom";
import Modal from 'react-bootstrap/Modal';
import Ratio from 'react-bootstrap/Ratio';

export const VideoPopUp:FC<{isModalOpen:boolean, setIsModalOpen:Dispatch<boolean>,videoSource:string|undefined}> = ({isModalOpen,setIsModalOpen,videoSource}) =>{
  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  },[]);

    return (
        <Modal show={isModalOpen} onHide={()=>{setIsModalOpen}} centered>
            <Modal.Body>
                <Link to="#" className="close" data-dismiss="modal" aria-label="Close" onClick={(e)=>{e.preventDefault();closeModal();}}>
                    <span aria-hidden="true">&times;</span>
                </Link>        
                    {/* <!-- 16:9 aspect ratio --> */}
                    <Ratio aspectRatio="16x9">
                        <iframe 
                            className="w-100 h-100"
                            src={String(videoSource)+"?autoplay=1&amp;modestbranding=1&amp;showinfo=0"} 
                            title="video" 
                            allowFullScreen
                        ></iframe>
                   </Ratio>
            </Modal.Body>
        </Modal>
    )
}

export const VideoPopUpTrigger:FC<{children:JSX.Element,videoUrl:string,setIsModalOpen:Dispatch<boolean>,setVideoSource:Dispatch<string|undefined>}> = ({children,videoUrl,setIsModalOpen,setVideoSource}) => {
    const openModal = useCallback(() => {
        setIsModalOpen(true);
        setVideoSource(videoUrl);
      },[videoUrl]);
    return (
        <Link to="#" data-toggle="modal" data-src={videoUrl} data-target="#videoModal" className="h-100" onClick={(e)=>{e.preventDefault();openModal()}}>
            {children}
        </Link>
    )
}