/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {FC, Dispatch, useContext,useState,useCallback} from "react";
import {request} from "../../../helpers/request"
import { HTTP } from "../../../helpers/constants/https";
// import ENUM_URL from "../../../helpers/constants/url";
import { Schema } from "joi";
import {UrlString} from "../../../models/form"
import { createHash } from "../../../helpers/cryptography";
import { RepositoryContext } from "../../../repositories/repositoryContext";

export const InputComponent:FC<{label:string,type:"text"|"password"|"email",fieldname:string,state:object,setState:Dispatch<{[key:string]:any}>,joi?:Schema}> = ({label,type,fieldname,state,setState,joi}) => {
    const [validationError,setValidationError] = useState<any>(undefined)

    const update = (event:{target:{value:any}}) => {
        if(joi){
            const {value:validatedValue,error:validationError} =  joi.validate(event.target.value)
            if(validationError){
                // console.log(validationError)
                setValidationError(validationError)
            }else{
                setValidationError(undefined)
                setState({...state, ...{[fieldname]:validatedValue}})
                
            }
        }
        if(!joi){ 
            setState({...state,...{[fieldname]:event.target.value}})
            setValidationError(undefined)
        }
    }
    return (
        <div className="mb-3">
            <div className="input-group">
                <span className="input-group-text" id="basic-addon1">{label}</span>
                <input className="form-control" placeholder={`please enter your ${label}`} type={type} onChange={update}></input>
            </div>
            <small className="text-danger form-group">{validationError?.message}</small>
        </div>
    )
}

export const SubmitComponent:FC<{iconClass?:string,label:string,joi?:Schema,url:UrlString,payload:{[key:string]:any},resHandler?:(res:any)=>void,className?:string}> = ({iconClass,label,joi,url,payload,resHandler,className}) => {
    const {errorRepository} = useContext(RepositoryContext)!
    const {setError} = errorRepository.errorRepo

    const submit = useCallback(async(e:{preventDefault:()=>void}):Promise<void> => {
        e.preventDefault()
        const payloadCopy = JSON.parse(JSON.stringify(payload)) as {[key:string]:any}
        Object.keys(payloadCopy).forEach(key=>{if(/(password|pswd)/i.test(key)){ payloadCopy[key] = createHash(String(payload[key])) }})
        console.log({payloadCopy})
        if(joi){
            const {error:validationError,value:validatedPayload} = joi.validate(payloadCopy)
            if(validationError){
                setError({ code:400, message: validationError?.message || "" })
            }
            if(!joi || (joi&&!validationError)){
                payload = validatedPayload || payloadCopy
                const {res,err} = await request<any>({
                    type:HTTP.POST,
                    url:url,
                    token:undefined,
                    payload,
                });
                if(err){
                    // console.table({err});
                    setError({ code:err.code || 400, message: err.detail as string || "" });
                }
                if(res){ 
                    // console.log({res})
                    if(resHandler)resHandler(res)
                }
                
            }
        }
    },[payload])
    return (
        <div className="form-group">
            <a className={className} onClick={async e => { await submit(e); }}>
                <i className={iconClass}></i> {label}
            </a>
        </div>
    )
}