import { FC } from "react";
import botImage from "/assets/chat/bot-icon.png";
import "./chat.css";
import {
  useFriendListingController,
} from "../../../controllers/message.controller";

const Dot: FC<{ color: string }> = ({ color }) => {
    return (
      <>
        <span
          style={{
            display: "inline-block",
            marginLeft: "5px",
            marginRight: "5px",
            marginBottom: "-2px",
            borderRadius: "50%",
            height: "15px",
            width: "15px",
            backgroundColor: color,
          }}
        />
      </>
    );
  };
  
const UnreadMessageCount:FC<{count?:number}> = ({count}) => {
    return (count&&count>0)?(
      <small style={{
       backgroundColor:"rgb(20, 227, 241)",
       color: "rgb(31, 28, 28)",
       width: "20px",
       height: "20px",
       textAlign: "center",
       borderRadius:"50%",
       fontSize: "12px"
      }}>{count}</small>
    ):(
      <></>
    )
}   

const FriendWidget: FC<{
    image?: string,
    name: string,
    online?: boolean,
    text: string,
    conversationID: string,
    unreadMessageCount?: number
    selectChatHandler: (conversationID: string) => Promise<void>;
  }> = ({ image, name, online, text, conversationID, unreadMessageCount, selectChatHandler }) => {

    return (
      <div
        key={conversationID}
        onClick={(e) => {
            e.preventDefault()
            void (async()=>{ await selectChatHandler(conversationID) })();
        }}
      >
        <div style={{ backgroundColor: "rgba(255,255,255,0.8)", margin: "5px" }}>
          <div
            className="row"
            key={"row-" + String(conversationID)}
          >
            <div className="col-3 text-center d-flex align-items-center">
              <img
                src={image || botImage}
                style={{
                  borderRadius: "50%",
                  width: "100px",
                  height: "100px",
                  alignItems: "center",
                }}
              />
            </div>
            <div className="col-9">
              {online ? (
                <div style={{ textAlign: "right" }}>
                  <strong>{name}</strong>
                  <Dot color="green" /> &nbsp;
                </div>
              ) : (
                <div style={{ textAlign: "right" }}>
                  <strong>{name}</strong>
                  <Dot color="grey" /> &nbsp;
                </div>
              )}
              <div style={{ height: "120px", overflow: "hidden", display:"flex" }}>
                <p className="p-1 d-flex align-items-center" style={{width:"90%"}}>
                  {text}
                </p>
                <div className="d-flex align-items-center p-1" style={{width:"10%"}}>
                  <UnreadMessageCount count={unreadMessageCount}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
  
  export const FriendListing: FC<{
    selectChat: (conversationID: string) => Promise<void>;
  }> = ({ selectChat }) => {
    const { users } = useFriendListingController();
    return (
      <>
        <div
          className="container customized-scrollbar"
          style={{ height: "550px", overflowY: "auto" }}
        >
          {users.map((user) => {
            return (
              <FriendWidget
                name={user.name}
                online={user.online}
                image={user.image}
                text={user.text}
                conversationID={user.conversationID}
                key={user.conversationID}
                selectChatHandler={selectChat}
                unreadMessageCount={user.unreadMessageCount}
              />
            );
          })}
        </div>
      </>
    );
  };