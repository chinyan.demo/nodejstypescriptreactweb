/* eslint-disable @typescript-eslint/no-misused-promises */
import { FC, useContext } from "react";
import image from "/assets/chat/images.jfif";
import "./chat.css";
import { IChat } from "../../../models/message";
import {
  useMessageSpaceController,
} from "../../../controllers/message.controller";
import { RepositoryContext } from "../../../repositories/repositoryContext";
import {SelfMessageBubble,FriendsMessageBubble} from "./MessageBubble"


export const MessageSpace: FC<IChat> = ({ users, chats, type, name }) => {
  const { bottomRef, auth, profileID } = useMessageSpaceController({ users, chats, type, name });
  const {selectedChatStates,chatListStates} = useContext(RepositoryContext)!.messageRepository
  const {allConversation} = chatListStates;
  const {conversationID} = selectedChatStates;
  return (
    <>
      <div
        className="customized-scrollbar"
        style={{
          backgroundColor: `rgba(0, 0, 0, 0.1)`,
          height: "100%",
          overflowY: "auto",
        }}
      >
        <div className="display-3">{name}</div>
        {name ? <hr /> : <></>}
        {allConversation[conversationID]?.chats.map(
          (
            msg: { sender: { profileID: string; name: string }; text: string },
            msgID
          ) => {
            if (msg.sender.profileID == profileID) {
              return (
                <SelfMessageBubble
                  name={String(auth.profile?.name)}
                  text={msg.text}
                  keyID={msgID.toString()}
                  key={"key" + String(msgID)}
                />
              );
            } else {
              return (
                <FriendsMessageBubble
                  name={msg.sender.name}
                  text={msg.text}
                  keyID={msgID.toString()}
                  key={"key" + String(msgID)}
                />
              );
            }
          }
        )}
      </div>
      <div ref={bottomRef}></div>
    </>
  );
};

export const UserInfo: FC<{ img?: string; name: string }> = ({ img, name }) => {
  return (
    <>
      <div className="d-flex p-5">
        <div className="container">
          <div className="row justify-content-center align-items-center">
            <img
              src={img || image}
              style={{
                borderRadius: "50%",
                width: "200px",
                height: "200px",
                justifyContent: "center !important",
                alignItems: "center",
              }}
            />
          </div>
          <div className="row">
            <p
              style={{
                color: "white",
                textAlign: "center",
                fontSize: "25px",
              }}
            >
              {name}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

