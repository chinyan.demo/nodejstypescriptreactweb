import {useMessageControllers} from "../../../controllers/message.controller";
import {FC} from "react";
import image from "/assets/chat/images.jfif";
import botImage from "/assets/chat/bot-icon.png";
import "./chat.css";

export const FriendsMessageBubble: FC<{
  name: string;
  text: string;
  imgUrl?: string;
  keyID?: string;
}> = ({ name, text, imgUrl, keyID }) => {
  const {bottomRef,scrollToBottom} = useMessageControllers()
  return (
    <>
      <div
        className="ms-1 mt-2 d-flex justify-content-start align-items-center"
        key={keyID}
        ref={bottomRef}
        onLoad={scrollToBottom} 
      >
        <img className="img-sm m-1" src={imgUrl || botImage} />
        <b>{name}</b>
      </div>
      <div className="d-flex justify-content-start">
        <div className="chat-bubble-guest m-1 d-flex">
          <div className="width:100%; text-align: left;">
            <p className="m-0 message items-align-center">{text}</p>
          </div>
        </div>
      </div>
    </>
  );
};

export const SelfMessageBubble: FC<{
  name: string;
  text: string;
  imgUrl?: string;
  keyID?: string;
}> = ({ name, text, imgUrl, keyID }) => {
  const {bottomRef,scrollToBottom} = useMessageControllers()
  return (
    <>
      <div
        className="ms-1 mt-2 d-flex justify-content-end align-items-center"
        key={keyID}
        ref={bottomRef}
        onLoad={scrollToBottom} 
      >
        <b>{name}</b>
        <img className="img-sm m-1" src={imgUrl || image} />
      </div>
      <div className="d-flex justify-content-end">
        <div className="chat-bubble-user m-1 d-flex">
          <div style={{ width: "100%", textAlign: "left" }}>
            <p className="m-0 message">{text}</p>
          </div>
        </div>
      </div>
    </>
  );
};