import {useMessageInputController} from "../../../controllers/message.controller";
import {FC} from "react";
import blackBgImg from "/assets/chat/bgImg-black.png";
import { Link } from "react-router-dom";
import { IncomingModel } from "../conference/incoming";
import { OutgoingModel } from "../conference/outgoing";
import "./chat.css";
import { ConferenceModel } from "../conference/conference";


export const MessagesInput: FC = () => {
  const { sendMessage, text, setTextStates, startVideoCall } = useMessageInputController();

  return (
    <div
      className="row d-flex align-items-center p-2"
      style={{
        height: "10% !important",
        backgroundImage: `url(${blackBgImg})`,
      }}
    >
      <div className="col-9">
        <input
          id="textInput"
          type="text"
          className="form-control ms-3"
          name="msg"
          placeholder="Message"
          value={text}
          onKeyUp={(e) => {
            e.preventDefault();
            if (e.key === "Enter") void (async()=>{ await sendMessage(); })()
          }}
          onChange={(e) => {
            e.preventDefault();
            setTextStates(e.target.value);
          }}
        />
      </div>
      <div className="col-2">
        <button
          className="btn btn-dark form-control"
          onClick={(e) => {
            e.preventDefault();
            void (async()=> {await sendMessage();})()
          }}
        >
          <i className="fa fa-paper-plane"></i> &nbsp; send
        </button>
      </div>
      <div className="col-1">
        <Link
          to="#"
          className="btn btn-primary"
          onClick={startVideoCall}
        >
          <i className="fas fa-video"></i>
        </Link>
      </div>
      <IncomingModel/>
      <OutgoingModel/>
      <ConferenceModel/>
    </div>
  );
};