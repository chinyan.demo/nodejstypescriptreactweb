/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {FC, useState, JSX, useEffect} from "react"
import nightRiver from "/assets/landing/nightRiver.jpg"
import nightKL from "/assets/landing/nightKL.jpg"
import nightPutraJaya from "/assets/landing/nightPutraJaya.jpg"
import routePng from "/assets/landing/route.png"
import { Link } from "react-router-dom";

const Slider: FC = () => {
    return (
        <div id="demo" className="carousel slide min-vh-100" data-bs-ride="carousel" style={{"height":"auto","width":"100%"}}>

            {/* <!-- Indicators/dots --> */}
            <div className="carousel-indicators">
                <button type="button" data-bs-target="#demo" data-bs-slide-to="0" className="active"></button>
                <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
                <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
            </div>

            
            {/* <!-- The slideshow/carousel --> */}
            <div className="carousel-inner min-vh-100" style={{"height":"100%","width":"100%"}}>
         
                    <div 
                        className="carousel-item active min-vh-100" 
                        style={{
                            backgroundImage: `url(${nightRiver})`,
                            backgroundRepeat:"no-repeat",backgroundSize:"cover", 
                            backgroundPosition:"center", width:"100%"
                        }}>
                        <div className="display-3" style={{textAlign:"center", paddingTop:"50px", paddingBottom:"-150px", color:"white"}}>
                            The All Nighters
                        </div>
                            <div className="carousel-caption">
                                <h1>River Of Life</h1>
                                <h6>Klang Valley</h6>
                                <p>Said to be one of the top ten waterfronts in the world, The River of Life is also one of the most iconic sights in Kuala Lumpur, dotted with quaint cafes, scenic bicycle lanes, and manicured gardens. Visit at night for captivating lights and stunning views of the city's oldest mosque, Masjid Jamek Sultan Abdul Samad. Don't miss the nightly light show, when the river transforms into a striking cobalt blue, and water and lights dance to various musical tracks. Check for showtimes before you visit.</p>
                            </div>
                    </div>
   
                     <div 
                        className="carousel-item active min-vh-100" 
                        style={{
                            backgroundImage: `url(${nightKL})`,
                            backgroundRepeat:"no-repeat",backgroundSize:"cover", 
                            backgroundPosition:"center", width:"100%"
                        }}>
                        <div className="display-3" style={{textAlign:"center", paddingTop:"50px", paddingBottom:"-150px", color:"white"}}>
                            The All Nighters
                        </div>
                            <div className="carousel-caption">
                                <h1>Lake Symphony</h1>
                                <h6>Kuala Lumpur Centre City</h6>
                                <p>Within KLCC Park, sited at the esplanade outside of Suria KLCC, lies the 10,000 sq ft man-made Lake Symphony. Two musical fountains display over 150 unique programmed animations in a magical performance of sound and water. KLCC Lake Symphony Light and Sound Water Fountain showtimes are 8pm, 9pm  and 10pm daily. However, KLCC Lake Symphony Water Fountain showtimes (Light only) are 7:30pm, 8:30pm and 9:30pm daily</p>
                            </div>
                        </div>
                
                    <div 
                        className="carousel-item active min-vh-100" 
                        style={{
                            backgroundImage: `url(${nightPutraJaya})`,
                            backgroundRepeat:"no-repeat", backgroundSize:"cover", 
                            backgroundPosition:"center", width:"100%"
                        }}>
                        <div className="display-3" style={{textAlign:"center", paddingTop:"50px", paddingBottom:"-150px", color:"white"}}>
                            The All Nighters
                        </div>
                            <div className="carousel-caption">
                                <h1>Sri Wawasan Bridge</h1>
                                <h6>Putrajaya</h6>
                                <p>The Seri Wawasan Bridge is one of the main bridges in the planned city Putrajaya, the new Malaysian federal territory and administrative centre. This futuristic asymmetric cable-stayed bridge with a forward-inclined pylon has a sailing ship appearance, accented at night with changeable color lighting.</p>
                            </div>
                        </div>
                
            </div>
            
            {/* <!-- Left arrow --> */}
            <button className="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
                <span className="carousel-control-prev-icon"></span>
            </button>

            {/* <!-- right arrow --> */}
            <button className="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
                <span className="carousel-control-next-icon"></span>
            </button>
        </div>
    )
}

const NavBar:FC=()=>{
    let initialState = {login:"nav-link active",registration:"nav-link"}
    if(sessionStorage.getItem("currentPage")) initialState = JSON.parse(sessionStorage.getItem("currentPage") as string)
    const [visit,setVisit] = useState(initialState)
    const visitPage = () => {
        const curUrl = window.location.href
        const registerPageUriRegexp = new RegExp(/register/)
        const newState = {
            login:(!registerPageUriRegexp.test(curUrl)) ? "nav-link active" : "nav-link",
            registration: (registerPageUriRegexp.test(curUrl)) ?  "nav-link active" : "nav-link"
        }
        sessionStorage.setItem("currentPage",JSON.stringify(newState))
        setVisit(newState)
      
    }
    useEffect(()=>{visitPage()},[])
    return (
        <ul className="nav nav-tabs m-3">
            <li className="nav-item">
                <Link className={visit.login} to="/" onClick={()=>{visitPage()}}>Login</Link>
            </li>
            <li className="nav-item">
                <Link className={visit.registration} to="/register/" onClick={()=>{visitPage()}}>Register</Link>
            </li>
        </ul>
    )
}

const Card:FC<{children:JSX.Element}> = ({children}) => {
    return (
        <div style={{
            display: "flex",
            justifyContent: "center", /* Center horizontally */
            alignItems: "center", /* Center vertically */
            height: "100%", /* Make sure the container fills the available height */
            margin:"0px 80px 0px 80px"
          }}>
            <div className="card mb-3" style={{width: "100%"}}>
                <div className="card-body text-dark">
                    <NavBar/>
                    {children}
                </div>
            </div>
        </div>
    )
}

export const LandingLayout: FC<{title:string, children:JSX.Element}> = ({ title, children }) => {
    return (
        <>
            <title>{title}</title>
            <div className="container-fluid" style={{height:"100%"}}>
                <div className="row min-vh-100" style={{display: "flex",alignItems: "stretch",height:"100%"}}>
                    <div className="col col-md-6 col-sm-12 p-0 m-0">
                        <Slider/>
                    </div>
                    <div className="col col-md-6 col-sm-12 justify-content-center align-items-center min-vh-100 p-0 m-0">
                        <div
                            style={{
                                width:"100%", 
                                height:"100%",
                                backgroundImage:`url(${routePng})`,
                                backgroundRepeat:"no-repeat",
                                backgroundSize:"cover", 
                                backgroundPosition:"center"
                            }}
                        >
                            <Card>{children}</Card>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
  }