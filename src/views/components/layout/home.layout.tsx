import { FC, useState, useEffect, JSX } from "react";
import { HomeTopNav } from "../navbar/topbar";
import { HomeSideNav } from "../navbar/sidebar";

const HomeLayout:FC<{children:JSX.Element}> = ({children}) => {
    const [currentUrl, setCurrentUrl] = useState("")
    useEffect(()=>{
        const curUrl = window.location.href.split("/private/")[1] 
        setCurrentUrl(curUrl)
        // console.log(`/private/home == /private/${currentUrl}`,"/private/home"==`/private/${currentUrl}`)
    })
    const sidebarData = [{
        icon:"fas fa-video",
        label:"Conference",
        url:"/private/home",
        active: new RegExp("/private/home").test(`/private/${currentUrl}`)
    },{
        icon:"fas fa-thumbtack",
        label:"Posts",
        url:"/private/post",
        active: new RegExp("/private/post").test(`/private/${currentUrl}`)
    },{
        icon:"fas fa-users",
        label:"Friends",
        url:"/private/friends",
        active: new RegExp("/private/friends").test(`/private/${currentUrl}`)
    },{
        icon:"fa fa-comments",
        label:"Messages",
        url:"/private/messages",
        active: new RegExp("/private/messages").test(`/private/${currentUrl}`)
    },{
        icon:"fas fa-film",
        label:"Video",
        url:"/private/video",
        active: new RegExp("/private/video").test(`/private/${currentUrl}`)
    },{
        icon:"fa fa-camera",
        label:"Gallery",
        url:"/private/gallery",
        active: new RegExp("/private/gallery").test(`/private/${currentUrl}`)
    }]
    return(
        <div>
            <HomeTopNav/>
            <HomeSideNav data={sidebarData}>
                {children}
            </HomeSideNav>
          
        </div>
    )
}

export default HomeLayout