import { FC, useContext } from "react"
import { PopupModal } from "../../components/bootstrap/PopupModal"
import { RepositoryContext } from "../../../repositories/repositoryContext"
import imgPath from "/assets/chat/unknownUser.png"
import { useConferenceInvitationController } from "../../../controllers/conference.controller"
import { Button } from "react-bootstrap"


export const IncomingModel:FC = () => {
    const { showIncomingModelStates, incomingDataStates } = useContext(RepositoryContext)!.conferenceRepository
    const { chatListStates } = useContext(RepositoryContext)!.messageRepository
    const { showIncoming, setShowIncoming} = showIncomingModelStates
    const {allConversation} = chatListStates
    const {incomingData} = incomingDataStates
    const name = incomingData? (allConversation[incomingData?.conversationID]?.name) : ""
    const {onAcceptInvitation} = useConferenceInvitationController()

    return (
        <PopupModal show={showIncoming} setShow={setShowIncoming}>
            <div className="container">
                {/* <div className="d-flex justify-content-center"> */}
                    <div className="row">
                        <div className="d-flex justify-content-center">
                            <p>you have a conference invitation</p>
                        </div>
                    </div>
                    <div className="row">  
                        <div className="d-flex justify-content-center">
                            <img src={imgPath} 
                            className="d-flex items-align-center"
                            style={{
                                width:"300px",
                                height:"300px",
                                borderRadius:"50%",
                                border: "1px solid black"
                            }}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="d-flex justify-content-center">
                            <p>{name}</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="d-flex justify-content-center">
                            <Button 
                                variant={"dark"} 
                                onClick={onAcceptInvitation}>
                                Accept 
                            </Button>
                        </div>
                    </div>
                {/* </div> */}
            </div>
        </PopupModal>
    )
}