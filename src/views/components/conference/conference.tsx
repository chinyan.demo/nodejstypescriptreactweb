import {FC,useContext,useState} from "react"
import { PopupModal } from "../bootstrap/PopupModal"
import { RepositoryContext } from "../../../repositories/repositoryContext"


const VideoComponent:FC = () => {
    return (
        <div style={{"width":"100%", "height":"100%"}}>
            <video style={{"width":"100%", "height":"100%"}}  autoPlay>
                Your browser does not support the video tag.
            </video>
        </div>
    )
}

const SmallVideoComponent:FC = () => {
    return (
        <div className="row">
            <video  autoPlay>
                Your browser does not support the video tag.
            </video>
        </div>
    )
}

export const ConferenceModel:FC = () => {
    const {conferenceRepository} = useContext(RepositoryContext)!
    const {showConference, setShowConference} = conferenceRepository.showConferenceStates
    const data = ["","","","","","","","","",""]

    return (
        <div>
            <PopupModal show={showConference} setShow={setShowConference} title="pirated zoom">
                <div className="row gx-0" style={{backgroundColor:"black"}}>
                    <div className="col-9"><VideoComponent/></div>
                    <div className="col-3 customized-scrollbar" style={{
                      overflowY: "auto",
                      overflowX: "hidden",
                      maxHeight: "60vh"
                    }}>
                        { data.map((_,id)=><SmallVideoComponent key={id}/>)}
                    </div>
                </div>
            </PopupModal>
        </div>
    )
}

export const ConferencePopUp:FC = () => {
    const [show,setShow] = useState<boolean>(true)
    const data = ["","","","","","","","","",""]

    return (
        <div>
            <PopupModal show={show} setShow={setShow} title="pirated zoom">
                <div className="row gx-0" style={{backgroundColor:"black"}}>
                    <div className="col-9"><VideoComponent/></div>
                    <div className="col-3 customized-scrollbar" style={{
                      overflowY: "auto",
                      overflowX: "hidden",
                      maxHeight: "60vh"
                    }}>
                        { data.map((_,id)=><SmallVideoComponent key={id}/>)}
                    </div>
                </div>
            </PopupModal>
        </div>
    )
}