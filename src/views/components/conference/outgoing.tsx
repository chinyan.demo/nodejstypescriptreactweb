import {FC, useCallback, useContext, useEffect} from "react"
import { PopupModal } from "../bootstrap/PopupModal"
import { RepositoryContext } from "../../../repositories/repositoryContext"
import image from "/assets/chat/images.jfif";
import { SocketContext } from "../../../helpers/websocket/socketProvider";
import { TOPIC } from "../../../helpers/constants/topics";
import { PeerContext } from "../../../helpers/webrtc/PeerProvider";
import Peer from "simple-peer";

export const OutgoingModel:FC = () => {
    const {conferenceRepository,messageRepository} = useContext(RepositoryContext)!
    const {showOutgoingModelStates,showConferenceStates} = conferenceRepository
    const {chatListStates,selectedChatStates} = messageRepository
    const {showOutgoing,setShowOutgoing} = showOutgoingModelStates
    const {allConversation} = chatListStates
    const {conversationID} = selectedChatStates
    const { setShowConference } = showConferenceStates
    const {socket} = useContext(SocketContext)
    const {peerRef} = useContext(PeerContext)

    const onConferenceInvitationAccepted = useCallback(()=>{
        peerRef!.current = new Peer({
            initiator: true,
            trickle:false,
          });
        setShowOutgoing(false)
        setShowConference(true)
    },[setShowOutgoing,setShowConference,peerRef])

    useEffect(()=>{
        socket?.on(TOPIC.CONFERENCE_INVITATION_ACCEPTED,onConferenceInvitationAccepted)
        return ()=>{ socket?.off(TOPIC.CONFERENCE_INVITATION_ACCEPTED)}
    },[socket,onConferenceInvitationAccepted])

    const name = (conversationID)? allConversation[conversationID]?.name : "" 
    return (
        <PopupModal show={showOutgoing} setShow={setShowOutgoing}>
             <div className="container">     
                <div className="row">
                    <div className="d-flex justify-content-center">
                        <p>sending your conference invitation</p>
                    </div>
                </div>
                <div className="row">  
                    <div className="d-flex justify-content-center">
                        <img src={image} 
                        className="d-flex items-align-center"
                        style={{
                            width:"300px",
                            height:"300px",
                            borderRadius:"50%",
                            border: "1px solid black"
                        }}/>
                    </div>
                </div>
                <div className="row">
                    <div className="d-flex justify-content-center">
                        <p>{name}</p>
                    </div>
                </div>
            </div>
        </PopupModal>
    )
}