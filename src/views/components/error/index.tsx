import { FC, useContext, JSX, useCallback} from "react";
import { initErrorStates } from "../../../repositories/states/error.states";
import { RepositoryContext } from "../../../repositories/repositoryContext";

export const ErrorHandler:FC<{children:JSX.Element}> = ({children}) => {
    // const { state:globalState, dispatch } = useContext(StatesContext);
    const {errorRepository} = useContext(RepositoryContext)!
    const {error,setError} = errorRepository.errorRepo;
    const {socketError,setSocketError} = errorRepository.socketErrorRepo;

    const removeErrorDisplay = useCallback(() => { setError(initErrorStates) },[setError]);
    const removeSocketErrorDisplay = useCallback(() => { setSocketError(initErrorStates) },[setSocketError]);

    return (
        <div className="p-0 m-0">
            <div className="alert alert-danger p-3 m-0" role="alert" 
            style={{
                display:(error.code!=200)?"block":"none",
                zIndex: "20"
            }}>
                <div className="d-flex row">
                    <div className="d-flex col col-md-6 justify-content-start">
                        <i className="fas fa-bell"></i> &nbsp;&nbsp;&nbsp; {error.message}
                    </div>
                    <div className="d-flex col col-md-6 justify-content-end">
                        <a href="#" onClick={removeErrorDisplay}>
                            <i className="fas fa-times" style={{color:"red"}}></i>
                        </a>
                    </div>
                </div>
            </div>
            <div className="alert alert-warning p-3 m-0" role="alert" 
                style={{
                    display:(socketError.code!=200)?"block":"none",
                    zIndex: "20"
                }}>
                <div className="d-flex row">
                    <div className="d-flex col col-md-6 justify-content-start">
                        <i className="fas fa-bell"></i> &nbsp;&nbsp;&nbsp; {sessionStorage.getItem("session")&&socketError.message}
                    </div>
                    <div className="d-flex col col-md-6 justify-content-end">
                        <a href="#" onClick={removeSocketErrorDisplay}>
                            <i className="fas fa-times" style={{color:"red"}}></i>
                        </a>
                    </div>
                </div>
            </div>
            {children}
        </div>
    )
}