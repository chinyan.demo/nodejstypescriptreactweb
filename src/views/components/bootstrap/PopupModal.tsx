import { FC, useCallback, Dispatch } from "react";
// import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export const PopupModal:FC<{show:boolean,setShow:Dispatch<boolean>,title?:string,children:JSX.Element}> = ({show,setShow,title,children}) => {
  const handleClose = useCallback(() => setShow(false),[setShow]);

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{children}</Modal.Body>
        <Modal.Footer>
          {/* <Button variant="secondary" onClick={handleClose}>
            Close
          </Button> */}
        </Modal.Footer>
      </Modal>
    </>
  );
}