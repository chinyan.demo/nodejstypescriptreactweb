import { FC,JSX } from "react";

export const Accordion:FC<{children:JSX.Element,title:string,description:string}> = ({children,title,description}) => {
    return (
        <div className="accordion" id="accordionPanelsStayOpenExample">
            <div className="accordion-item">
                <h2 className="accordion-header" id="panelsStayOpen-headingOne">
                    <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        <strong>{title}</strong>
                    </button>
                </h2>
                <div id="panelsStayOpen-collapseOne" className="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                <div className="accordion-body">
                    <strong>{description}</strong>
                    <br/><br/>
                    {children}
                </div>
            </div>
        </div>
    </div>
    )
}
