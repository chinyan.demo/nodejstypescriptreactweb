/* eslint-disable @typescript-eslint/no-unnecessary-type-assertion */
import  { FC, useEffect, useState, Dispatch } from "react";
import { Link } from "react-router-dom";
import no_image_available from "/assets/no_image_available.jpg"
import { VideoPopUp, VideoPopUpTrigger } from "../video/popUp";

interface IImageCard {imageSrc?:string,title:string,description:string,idx?:string,url?:string}

export const ImageCard:FC<IImageCard> = ({imageSrc,title,description,idx,url}) => {
    return (
        <Link to={url||"#"} className="no-underline-link">
            <div className="card hoverZoom h-100" key={idx} >
                <img className="img-fluid h-100" alt="100%x280" src={imageSrc||no_image_available}/>
                <div className="card-img-overlay text-white d-flex flex-column justify-content-end">
                    <h4 className="card-title">{title}</h4>
                    <p className="card-text">{description}</p>
                </div>
            </div>
        </Link>
    )
}

export const VideoCard:FC<IImageCard&{setIsModalOpen:Dispatch<boolean>,setVideoSource:Dispatch<string|undefined>}> = ({imageSrc,title,description,idx,url,setIsModalOpen,setVideoSource}) => {
    return (
        <VideoPopUpTrigger videoUrl={url||""} setIsModalOpen={setIsModalOpen} setVideoSource={setVideoSource}>
             <div className="card hoverZoom h-100" key={idx} >
                <img className="img-fluid h-100" alt="100%x280" src={imageSrc||no_image_available}/>
                <div className="card-img-overlay text-white d-flex flex-column justify-content-end">
                    <h4 className="card-title">{title}</h4>
                    <p className="card-text">{description}</p>
                </div>
            </div>
        </VideoPopUpTrigger>
    )
}

export const CardSliderCol3:FC<{data:Array<IImageCard>}> = ({data}) => {
    const [dataGroups,setDataGroups] = useState<Array<IImageCard[]|undefined[]>>([])
    const [isModalOpen,setIsModalOpen]  = useState<boolean>(false);
    const [videoSource, setVideoSource] = useState<string>()

    useEffect(()=>{
        while(data.length%3!=0||data.length==0){
            data.push({
                title:"No Conference Yet",
                description:"No Conference Yet",
                idx:JSON.stringify(data.length)
            });
        }
    
        let DataGroup:Array<IImageCard|undefined> = []
        while(data.length!=0){
            DataGroup.push(data.shift())
            if(DataGroup.length==3){
                // eslint-disable-next-line @typescript-eslint/no-unsafe-return
                setDataGroups((prevState:Array<IImageCard[]|undefined[]>)=>[...prevState ,JSON.parse(JSON.stringify(DataGroup))])
                DataGroup = []
            }
        }
    },[data])
   


    return (
    <>
        <div className="col-12">
            <div id="carouselExampleIndicators2" className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                        {
                            dataGroups.map((groupedData:Array<IImageCard|undefined>,idx)=>{
                                return (
                                    <div className={(idx ==0)?"carousel-item active":"carousel-item"} key={"group"+String(idx)}>
                                        <div className="row">
                                            {
                                                groupedData.map((unitData,dataID)=>{
                                                    return (
                                                        <div className="col-md-4 mb-3" key={"card"+JSON.stringify(dataID)}>
                                                            {
                                                                (unitData?.url)?(
                                                                    <ImageCard 
                                                                        imageSrc={unitData?.imageSrc||no_image_available} 
                                                                        title={unitData?.title as string} 
                                                                        description={unitData?.description as string} 
                                                                        key={JSON.stringify(dataID)}
                                                                    />
                                                                ):(
                                                                    <VideoCard 
                                                                        imageSrc={unitData?.imageSrc||no_image_available} 
                                                                        title={unitData?.title as string} 
                                                                        description={unitData?.description as string} 
                                                                        key={JSON.stringify(dataID)}
                                                                        url="https://www.youtube.com/embed/WmR9IMUD_CY"
                                                                        setIsModalOpen={setIsModalOpen}
                                                                        setVideoSource={setVideoSource}
                                                                    />
                                                                )
                                                            }
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                )
                            })
                        }
                        {/* <!-- Left arrow --> */}
                        <button className="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon"></span>
                        </button>
                        {/* <!-- right arrow --> */}
                        <button className="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
                            <span className="carousel-control-next-icon"></span>
                        </button>
                </div>
            </div>
        </div>
        <VideoPopUp setIsModalOpen={setIsModalOpen} isModalOpen={isModalOpen} videoSource={videoSource}/>
    </>
    )
}