/* eslint-disable @typescript-eslint/no-misused-promises */
// import { useWebRTC } from "../../../helpers/websocket/webrtc";
import "./style.css";
import { FC, useRef, useContext } from "react";
import { PopupModal } from "../bootstrap/PopupModal";
import { SocketContext } from "../../../helpers/websocket/socketProvider";
import { RepositoryContext } from "../../../repositories/repositoryContext";
import { useVideoCallController } from "../../../controllers/videocall.controller";
import { Button } from "react-bootstrap";

export const VideoGrid: FC<{ children: JSX.Element }> = ({ children }) => {
  return <div className="video-grid">{children}</div>;
};

export const Video: FC<{ mediaStream: MediaStream }> = ({ mediaStream }) => {
  const videoRef = useRef<HTMLVideoElement>(null);

  if (mediaStream && videoRef.current && !videoRef.current.srcObject) {
    videoRef.current.srcObject = mediaStream;
  }
  return (
    <div>
      <video
        ref={videoRef}
        onCanPlay={async () => {
          await videoRef.current?.play();
        }}
        className="video"
      />
    </div>
  );
};

export const VideoContainer: FC<{
  peerMediaStreams: { [key: string]: MediaStream };
  mediaStream: MediaStream;
}> = ({ peerMediaStreams, mediaStream }) => {
  return (
    <VideoGrid>
      <>
        <Video mediaStream={mediaStream} />
        {Object.keys(peerMediaStreams).map((key) => {
          return <Video mediaStream={peerMediaStreams[key]} />;
        })}
      </>
    </VideoGrid>
  );
};

export const VideoCallModal: FC = () => {
  const { socket } = useContext(SocketContext);
  const { videoCallRepository } = useContext(RepositoryContext)!;
  const { incoming, setIncoming } = videoCallRepository.inComingRepo;
  const { outGoing } = videoCallRepository.outGoingRepo;
  const { show, setShow } = videoCallRepository.popUpRepo;

  // const { videoStream, peerVideoStream } = useWebRTC();
  // const profile = state[STATES.AUTH][0].profile
  socket?.on(
    "callUser",
    (data: { signal: any; profileID: string; name: string }) => {
      const { profileID, name } = data;
      setShow(true);
      setIncoming({ profileID, name });
    }
  );
  return (
    <PopupModal show={show} setShow={setShow} title="Video Call">
      <>
        {incoming ? (
          <>{incoming.name} is calling</>
        ) : outGoing ? (
          <>calling {outGoing.name}</>
        ) : (
          <></>
        )}
        {/* <VideoContainer
          // peerMediaStreams={peerVideoStream}
          // mediaStream={videoStream as MediaStream}
        /> */}
      </>
    </PopupModal>
  );
};

export const TestVideoCallComponent: FC = () => {
  const { videoCallRepository, authRepository } =
    useContext(RepositoryContext)!;
  const { auth } = authRepository.authStates;

  const { receivingCall } = videoCallRepository.receivingCallRepo;
  const { callAccepted } = videoCallRepository.callAcceptedRepo;
  const { idToCall, setIdToCall } = videoCallRepository.idToCallRepo;
  const { callEnded } = videoCallRepository.callEndedRepo;
  const { caller } = videoCallRepository.callerRepo;
  const { onlineUser } = videoCallRepository.onlineuserStates;

  const { myVideo, userVideo, leaveCall, callUser, answerCall } =
    useVideoCallController();

  return (
    <>
      <div> Video Call</div>
      <div className="myId">
        <input
          type="text"
          placeholder="name"
          value={auth.profile?.name}
          style={{ marginBottom: "20px" }}
        />

        <select value={idToCall} onChange={(e) => setIdToCall(e.target.value)}>
          {onlineUser?.map(
            (online_user: { profileID: string; name: string }) => {
              return (
                <option
                  key={online_user.profileID}
                  value={online_user.profileID}
                >
                  {online_user.name}
                </option>
              );
            }
          )}
        </select>

        <div className="call-button">
          {callAccepted && !callEnded ? (
            <Button variant="contained" color="secondary" onClick={leaveCall}>
              End Call
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              aria-label="call"
              onClick={() => callUser(idToCall)}
            >
              <i className="fas fa-phone"></i> call {idToCall}
            </Button>
          )}
        </div>

        <div>
          {receivingCall && !callAccepted ? (
            <div className="caller">
              <h1>{caller?.name} is calling...</h1>
              <Button variant="contained" color="primary" onClick={answerCall}>
                Answer
              </Button>
            </div>
          ) : null}
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col">
            {/* {stream && <video playsInline muted ref={myVideo} autoPlay style={{width: "300px"}}/>} */}
            <video
              playsInline
              muted
              ref={myVideo}
              autoPlay
              style={{ width: "300px" }}
            />
          </div>
          <div className="col">
            {callAccepted && !callEnded ? (
              <video
                playsInline
                ref={userVideo}
                autoPlay
                style={{ width: "300px" }}
              />
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
};
