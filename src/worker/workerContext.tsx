import { FC, createContext, JSX, useState, useEffect, useContext, useCallback, useMemo } from "react";
import { RepositoryContext } from "../repositories/repositoryContext";
import { refreshTokenService } from "../repositories/services/auth.service";

type WorkerContextType = {
    backgroundWorker:Worker|undefined,
}

export const WorkerContext = createContext<WorkerContextType>({backgroundWorker:undefined});

export const WorkerProvider:FC<{children:JSX.Element}>  = ({children}) => {
    const {authStates,} = useContext(RepositoryContext)!.authRepository
    const {errorRepo,} = useContext(RepositoryContext)!.errorRepository
    const { auth, setAuth } = authStates;
    const {setError} = errorRepo;
    // const [bgWorker, setBgWorker] = useState<Worker|undefined>(undefined)

    const updateNewToken = useCallback(async(profileID:string,refreshToken:string)=>{
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            const {res,err} = await refreshTokenService(profileID,refreshToken)
            if(err) setError({message:JSON.stringify(err)})
            if(res) setAuth({...auth,...res})
    },[setError,setAuth,auth])
    
    const bgWorker = useMemo(()=>{
        console.log("worker created")
        return new Worker(new URL("./backgroundWorker.ts", import.meta.url))
    },[])
    useEffect(()=>{
        // setBgWorker(new Worker(new URL("./backgroundWorker.ts", import.meta.url)))
        // if(bgWorker)console.log("worker created")
        
        bgWorker?.addEventListener("message",(e:MessageEvent<{topic:string, payload:{[key:string]:any}}>)=>{
            console.log("__ON_MESSAGE__")
            switch(e.data.topic){
                case "REFRESH_TOKEN":{
                    if(auth?.profile&&auth.token){
                        console.log({auth})
                        const profileID : string = auth.profile.id;
                        const refreshToken:string = auth.token.refreshToken;
                        void ( async()=>{ await  updateNewToken(profileID,refreshToken) })()
                        
                        console.log({GET_TOKEN:""})
                    }
                    break;
                }
                default:
            }
        });
        // return () => {
        //     bgWorker?.terminate();
        //     console.log("worker terminated")
        // };

    },[])
    return(
        <WorkerContext.Provider value = {{
            backgroundWorker : bgWorker
        }}>
            {children}
        </WorkerContext.Provider>
    )
} 