import {createBrowserRouter, RouterProvider} from "react-router-dom"
import RegistrationPage from "../views/pages/register.page";
import LoginPage from "../views/pages/login.page";
import {Error404} from "../views/components/layout/error404.layout"
import { Middleware } from "./middleware";
import ConferencePage from "../views/pages/private/conference/conference.page";
import MessagesPage from "../views/pages/private/message/message.page";
import  {TestVideoCallComponent} from "../../src/views/components/videocall";
import SamplePage from "../views/pages/sample.page";

const router = createBrowserRouter([
    {
        path:"/",
        element: <LoginPage/>
    },{
        path:"/register",
        element: <RegistrationPage/>
    },{
        path: "*",
        element: <Error404/>
    },{
        path: "/private",
        element: <Middleware/>,
        children:[
            {
                path: "/private/home",
                element:<ConferencePage/>
            },{
                path: "/private/messages",
                element: <MessagesPage/>
            },{
                path: "/private/video",
                element: <TestVideoCallComponent/>
            },{
                path: "/private/gallery",
                element: <SamplePage/>
            }
        ]
    }
]);

const MainRouter = () => {
    return (
        <><RouterProvider router={router}/></>
    )
}

export default MainRouter;