import { FC,useContext,useEffect } from "react"
import { Outlet,useNavigate  } from "react-router-dom"
import HomeLayout from "../views/components/layout/home.layout"
import { SocketProvider } from "../helpers/websocket/socketProvider"
import { RepositoryContext } from "../repositories/repositoryContext"
import { PeerProvider } from "../helpers/webrtc/PeerProvider"

export const Middleware:FC = () => {
    const {authRepository} = useContext(RepositoryContext)!
    const { auth } = authRepository.authStates;
    const navigate = useNavigate()
    useEffect(()=>{
        if(!auth.profile?.id){
            navigate("/")
        }

    },[auth,navigate])
    
    return (
        <>
            <SocketProvider>
                <PeerProvider>
                    <HomeLayout>
                        <Outlet/>
                    </HomeLayout>
                </PeerProvider>
            </SocketProvider>
        </>
    )
}