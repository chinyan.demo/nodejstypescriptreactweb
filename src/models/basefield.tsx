export interface IBaseField   {
  _id : string
  _key: string
  _rev?: string

  active? : boolean
  createdAt: string
  createdBy: string
  updatedAt: string
  updatedBy: string
}


  
