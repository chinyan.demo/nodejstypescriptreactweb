import { Dispatch,SetStateAction } from "react";

export interface IError {
    code?: number,
    message: string,
    info?: string
}

export interface IErrorRepo {
    errorRepo: { error:IError, setError:Dispatch<SetStateAction<IError>>},
    socketErrorRepo: { socketError:IError, setSocketError:Dispatch<SetStateAction<IError>>}
}