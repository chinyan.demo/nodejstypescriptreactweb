import { Dispatch,SetStateAction } from "react";
import { IBaseField } from "./basefield";

type ChatType = "private_message" | "group_chat";
export interface IChat {
  users: Array<{ profileID: string; name: string }>;
  chats: Array<{
    messageID?:string,
    sender: { name: string; profileID: string };
    text: string;
    read?: boolean;
  }>;
  lastChat?: string;
  unreadMessagesCount?:number
  type: ChatType;
  name: string;
}

export interface IAllConversations {
  [conversationID: string]: IChat
}

export interface IMessagesRepository {
  selectedChatStates: {
    conversationID: string;
    setConversationID: Dispatch<SetStateAction<string>>;
  };
  chatListStates: {
    allConversation: IAllConversations;
    setAllConversation: Dispatch<SetStateAction<IAllConversations>>;
  };
  userStates: {
    users: Array<IUser>;
    setUsers: Dispatch<SetStateAction<Array<IUser>>>;
  }
}

export interface IFetchedConversation extends IBaseField {
  name: string;
  type: ChatType;
  lastMessage?: {sender:string, text:string, datetime:string};
  unreadMessagesCount:{[profileID:string]:number}
  users: Array<{ profileID: string; name: string }>;
  profileIDs: Array<string>;
}

export interface IFetchMessages extends IBaseField {
  messageID?:string,
  conversationID: string;
  content: string;
  senderProfileID: string;
  readBy: Array<string>;
  sender: { name: string; profileID: string };
}

export interface IUser {
  conversationID: string;
  receiverProfileIDs: Array<string>;
  name: string;
  online: boolean;
  text: string;
  unreadMessageCount?: number;
  image?: string;
}
