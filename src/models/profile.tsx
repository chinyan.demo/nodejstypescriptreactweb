import { BASE_FIELD } from "../helpers/constants/states"

export interface IProfile {
    [BASE_FIELD.id]:string
    name: string
    email: string
}