import {HTTP} from "../helpers/constants/https" 

export type UrlString = `${"http"|"https"}://${string}`

export enum FORM{
    TEXT = "text",
    PASSWORD = "password",
    SUBMIT = "submit"
}

export enum EVENT{
    ON_CLICK = "ON_CLICK",
    ON_CHANGE = "ON_CHANGE",
    ON_MOUSE_OVER = "ON_MOUSE_OVER"
}

export type HttpMethodType = HTTP.GET|HTTP.POST|HTTP.PUT|HTTP.DELETE
