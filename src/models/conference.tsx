import { Dispatch, SetStateAction } from "react";
import { Instance } from "simple-peer"

export type PeerComponent =  {profileID:string, name:string, peer?:Instance, video:boolean, audio:boolean, initiator:boolean };

export type IncomingData = {conversationID:string,message:{[key:string]:any}, hostProfileID:string}
export interface IConferenceRepository {
    peerUsersStates : {peerUsers:Array<PeerComponent>, setPeerUsers:Dispatch<SetStateAction<Array<PeerComponent>>>},
    focusedStates : {focusProfileID:string, setFocusProfileID:Dispatch<SetStateAction<string>>},
    hostStates : {hostProfileID:string,setHostProfileID:Dispatch<SetStateAction<string>>},
    conferenceEndedStates : {ended:boolean, setEnded:Dispatch<SetStateAction<boolean>>},
    showConferenceStates: {showConference:boolean, setShowConference:Dispatch<SetStateAction<boolean>>},
    showIncomingModelStates: {showIncoming:boolean, setShowIncoming:Dispatch<SetStateAction<boolean>>},
    showOutgoingModelStates: {showOutgoing:boolean, setShowOutgoing:Dispatch<SetStateAction<boolean>>},
    incomingDataStates : {incomingData:IncomingData|undefined, setIncomingData:Dispatch<SetStateAction<IncomingData|undefined>>}
} 