import { IProfile } from "./profile"
import { Dispatch,SetStateAction } from "react"

export interface IAuth {
   profile?: IProfile,
   token?: {
    accessToken:string,
    refreshToken:string,
   }
}

export interface IAuthRepo {
   authStates: {auth:IAuth, setAuth: Dispatch<SetStateAction<IAuth>>}
}