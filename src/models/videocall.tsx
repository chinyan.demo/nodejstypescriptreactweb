import { Dispatch,SetStateAction } from "react";

export type CallingComponent =  {profileID:string,name:string}|undefined;

export interface IVideoCallRepository {
    inComingRepo:{incoming:CallingComponent,setIncoming:Dispatch<SetStateAction<CallingComponent>>},
    outGoingRepo:{outGoing:CallingComponent,setOutGoing:Dispatch<SetStateAction<CallingComponent>>},
    popUpRepo:{show:boolean, setShow:Dispatch<SetStateAction<boolean>>}

    socketRepo: {socketID:string, setSocketID:Dispatch<SetStateAction<string>>},
    streamRepo: {stream:MediaStream|undefined, setStream:Dispatch<MediaStream>},
    receivingCallRepo : {receivingCall:boolean, setReceivingCall:Dispatch<boolean>},
    callerRepo : {caller:{name:string,profileID:string}, setCaller:Dispatch<SetStateAction<{name:string,profileID:string}>>},
    callerSignalRepo: {callerSignal:MediaStream|undefined, setCallerSignal:Dispatch<MediaStream>},
    callAcceptedRepo: {callAccepted:boolean, setCallAccepted:Dispatch<SetStateAction<boolean>>},
    idToCallRepo: {idToCall:string, setIdToCall:Dispatch<SetStateAction<string>>},
    callEndedRepo: {callEnded:boolean, setCallEnded:Dispatch<SetStateAction<boolean>>},
    nameRepo: {name:string, setName:Dispatch<SetStateAction<string>>}
    onlineuserStates: {onlineUser:Array<{name:string,profileID:string}>, setOnlineUser:Dispatch<SetStateAction<Array<{name:string,profileID:string}>>>} 
} 