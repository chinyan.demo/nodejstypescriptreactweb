import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { RepositoryContext } from "../repositories/repositoryContext";

export const useLoginController = () => {
    const [formData, setFormData] = useState({})
    const {errorRepository,authRepository} = useContext(RepositoryContext)!
    const {setError} = errorRepository.errorRepo;
    const {setAuth} = authRepository.authStates;

    const navigate = useNavigate();
    
    const loginHandler = (res: any) => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        const {success,data,errorObject} = res as {
            success:boolean,
            data?:{
                profile:{_id:string,name:string,email:string},
                token:{accessToken:string,refreshToken:string}
            },
            errorObject?:any
        };
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
        console.log({res})
        if(!success){ 
            console.log({ code:400, message: String(errorObject) })
            setError({ code:400, message: String(errorObject) }) 
        }
        if(success){
            setAuth({
                profile:{
                    id: data?.profile?._id as string,
                    name: data?.profile?.name as string,
                    email: data?.profile?.email as string
                },
                token: data?.token
            })
    
            sessionStorage.setItem("session",JSON.stringify({
                profile:{
                    id: data?.profile?._id,
                    name: data?.profile?.name,
                    email: data?.profile?.email
                },
                token: data?.token
            }))

            navigate("/private/home")
        }

        
    }

    return {formData,setFormData,loginHandler,navigate}
}

