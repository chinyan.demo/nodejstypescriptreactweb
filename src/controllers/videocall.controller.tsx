/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useContext, useEffect, useRef } from "react"
import { RepositoryContext } from "../repositories/repositoryContext"
import { SocketContext } from "../helpers/websocket/socketProvider"
import { PEER_TOPIC, TOPIC } from "../helpers/constants/topics"
import Peer, {Instance} from "simple-peer"
import { useOnlineUserFetch } from "../repositories/services/message.services"

export const useVideoCallController = () => {
    const {videoCallRepository, errorRepository,authRepository} = useContext(RepositoryContext)!
    const {socket} = useContext(SocketContext)

    const { setSocketID} = videoCallRepository.socketRepo
    const {stream, setStream} = videoCallRepository.streamRepo
    const { setReceivingCall} = videoCallRepository.receivingCallRepo
    const {caller, setCaller} = videoCallRepository.callerRepo
    const {callerSignal, setCallerSignal} = videoCallRepository.callerSignalRepo
    const { setCallAccepted} = videoCallRepository.callAcceptedRepo
    const { setCallEnded} = videoCallRepository.callEndedRepo
    const { onlineUser,setOnlineUser} = videoCallRepository.onlineuserStates
    const { setIdToCall} = videoCallRepository.idToCallRepo
    const {auth} = authRepository.authStates
    const { setError} = errorRepository.errorRepo

    const myVideo = useRef<HTMLVideoElement>(null)
    const userVideo = useRef<HTMLVideoElement>(null)
    const connectionRef = useRef<Instance>()

    const {data:fetchedOnlineUser} = useOnlineUserFetch<Array<{profileID:string,name:string}>>()

    useEffect(()=>{
        navigator.mediaDevices.getUserMedia({video: true, audio:true}).then((stream:MediaStream)=>{
            setStream(stream)
            if (myVideo.current)  myVideo.current.srcObject = stream
        }).catch((error: Error) => {
            setError(error);
        });

        socket?.on(TOPIC.SOCKET_ID,(id:string)=>{
            setSocketID(id)
        })

        socket?.on(TOPIC.CALL_USER,(data:{signalData:MediaStream, callerProfileID:string , callerName:string, profileID:string})=>{
            const {signalData, callerProfileID , callerName} = data
            console.table(data)
            setReceivingCall(true);
            setCaller({name:callerName,profileID:callerProfileID})
            setCallerSignal(signalData)
        })

        if(fetchedOnlineUser&&fetchedOnlineUser.length>0) fetchedOnlineUser.forEach((user:{profileID:string,name:string}) => {
            if(!onlineUser.find((online_user:{profileID:string,name:string})=>online_user.profileID==user.profileID)){
                if(user.profileID!=auth.profile?.id){
                    setOnlineUser([...onlineUser,user])
                }
            }
            if(onlineUser&&onlineUser.length>0) setIdToCall(onlineUser[0]?.profileID)
        });
        
        
    },[fetchedOnlineUser,socket,setIdToCall,myVideo,auth.profile?.id,setCaller,setReceivingCall,setCallerSignal,setError,setSocketID,setStream,setOnlineUser])

    const callUser = (id:string) => {
        const peer = new Peer({
            initiator: true,
            trickle:false,
            stream: stream
        });

        peer.on(PEER_TOPIC.SIGNAL, data => {
            socket?.emit(TOPIC.CALL_USER,{
                userToCall: id,
                signalData: data,
                callerProfileID: auth.profile?.id,
                callerName: auth.profile?.name
            })
        })

        peer.on(PEER_TOPIC.STREAM,(stream:MediaStream)=>{
            if( userVideo.current) userVideo.current.srcObject = stream
        })

        socket?.on(TOPIC.CALL_ACCEPTED,(signal)=>{
            setCallAccepted(true)
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            peer.signal(signal)
        })

        connectionRef.current = peer
    }

    const answerCall = () => {
        setCallAccepted(true)
        const peer = new Peer({
            initiator:false,
            trickle:false,
            stream:stream
        })

        peer.on(PEER_TOPIC.SIGNAL,data=>{
            console.log({signal: data, to: caller?.profileID})
            socket?.emit(TOPIC.ANSWER_CALL,{signal: data, to: caller?.profileID})
        })

        peer.on(PEER_TOPIC.STREAM, stream => {
            if(userVideo.current) userVideo.current.srcObject = stream
        })

        peer.signal(JSON.stringify(callerSignal))
        connectionRef.current = peer
    }

    const leaveCall = () => {
        setCallEnded(true)
        if(connectionRef.current)connectionRef.current.destroy()
    }

    return {myVideo,userVideo,connectionRef, callUser, answerCall, leaveCall}
    
    
}