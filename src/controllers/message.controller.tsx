/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { useState, useContext, useRef, useCallback, useEffect, MouseEventHandler } from "react";
import { SocketContext } from "../helpers/websocket/socketProvider";
import { IAllConversations, IChat, IFetchMessages, IFetchedConversation } from "../models/message";
import { TOPIC } from "../helpers/constants/topics";
import { superagentReq } from "../helpers/request";
import { HTTP } from "../helpers/constants/https";
import ENUM_URL from "../helpers/constants/url";
import { RepositoryContext } from "../repositories/repositoryContext";
import { updateMessageReadByService, useFetchConversationService } from "../repositories/services/message.services";
import useSession from "../helpers/hooks/session.hook";
import { PeerContext } from "../helpers/webrtc/PeerProvider";
import Peer from "simple-peer";

const uniqBy = (arr: Array<{[key:string]:any}>, predicate: string) => {
  if (!Array.isArray(arr)||arr.length==0) { return [];}

  const pickedObjects = arr.reduce((accumulated, next) => {
      const predicateValue = next[predicate];
      if (!predicateValue) return accumulated;
      
      return accumulated.has(predicateValue) ? accumulated : accumulated.set(predicateValue, next);
    }, new Map()).values();
  return [...pickedObjects];
};

export const useMessageControllers = () => {
  const { socket } = useContext(SocketContext);
  const { messageRepository, authRepository, errorRepository } = useContext(RepositoryContext)!;
  const { conversationID, setConversationID } = messageRepository.selectedChatStates;
  const { allConversation, setAllConversation } = messageRepository.chatListStates;
  const { users,setUsers } = messageRepository.userStates;
  const { setError } = errorRepository.errorRepo;
  const { auth } = authRepository.authStates;

  const bottomRef = useRef<HTMLDivElement>(null);

  const scrollToBottom = useCallback(() => {
    bottomRef.current?.scrollIntoView({ behavior: "smooth" });
  }, []);

  const selectChat = useCallback(
    async (conversationId: string) => {
      if (conversationId != "bot") {
        const { res: messages, err } = await superagentReq<Array<IFetchMessages>>({
          type: HTTP.POST,
          url: ENUM_URL.MESSAGE.LIST,
          token: auth.token?.accessToken,
          payload: {
            conversationID: conversationId,
            pagination: { offset: 0, limit: 100 },
          },
        });
        if (err) setError({ message: JSON.stringify(err) });

        console.table({ messages });

        const thisConversation:IChat = {
          users: allConversation[conversationId]?.users || [],
          chats: messages ? messages.map((msg: IFetchMessages) => {
              return {
                messageID:msg.messageID,
                sender: msg.sender,
                text: msg.content || "",
                read: msg.readBy.find((id) => id == auth.profile?.id)
                  ? true
                  : false,
              };
            }) : [],
          lastChat: !messages || messages.length == 0
              ? allConversation[conversationId]?.lastChat
              : messages[messages.length - 1].content,
          type: allConversation[conversationId]?.type,
          name: allConversation[conversationId]?.name || "",
        }

        setAllConversation({
          ...allConversation,
          ...{
            [conversationId]: thisConversation,
          },
        });
        setConversationID(conversationId);
        sessionStorage.setItem("conversationID",conversationId)
        setUsers(prev=>prev.map(user=>{
          return (user.conversationID == conversationId)? {...user,...{unreadMessageCount:0}}:user
        }))
        sessionStorage.setItem("users",JSON.stringify(users))
        await updateMessageReadByService({conversationID:conversationId,profileID:String(auth.profile?.id)},String(auth.token?.accessToken))
      }
      
    },
    [setConversationID,users,setUsers,setAllConversation,setError,auth.profile?.id, auth.token?.accessToken, allConversation]
  );

  const privateMessageHandler = useCallback((msg: {
    messageID?: string,
    senderName?: string,
    conversationID: string;
    content: string;
    senderProfileID: string;
    readBy?: Array<string>
    unreadMessagesCount?:{[profileID:string]:number}
  }) => {
    
    console.log("called : ",msg);
    console.log({allConversation})
    const newAllConversation = {
      ...allConversation,
      ...{
        [msg.conversationID]: {
          users: allConversation[msg.conversationID]?.users || [],
          chats: [...allConversation[msg.conversationID]&&allConversation[msg.conversationID].chats,{
            sender:{
              name:msg.senderName as string,
              profileID:msg.senderProfileID
            },
            text:msg.content, 
            read:msg.conversationID==conversationID
          }],
          lastChat: msg.content,
          type: allConversation[msg.conversationID].type,
          name: allConversation[msg.conversationID].name || "",
        },
      },
    }
    sessionStorage.setItem("allConversation",JSON.stringify(newAllConversation))
    setAllConversation(newAllConversation)
    setUsers(prev=>prev.map(user=>{
      const newUsers =  (user.conversationID==conversationID)?{
        conversationID,
        receiverProfileIDs: user.receiverProfileIDs,
        name:user.name,
        text:  msg.content,
        online:user.online,
        unreadMessageCount: (msg.unreadMessagesCount)?msg.unreadMessagesCount[String(auth.profile?.id)]:0
      } : user
      sessionStorage.setItem("users",JSON.stringify(newUsers))
      return newUsers
    }));
    socket?.off(TOPIC.PRIVATE_MESSAGE,privateMessageHandler);
  },[conversationID,allConversation,setAllConversation,socket,auth.profile?.id,setUsers]);

  useEffect(()=>{

    socket?.off(TOPIC.BROADCAST);
    socket?.on(TOPIC.BROADCAST, (data) => {
      console.log(`TOPIC.BROADCAST : `,data);
    });

    

    // socket?.off(TOPIC.PRIVATE_MESSAGE);
    socket?.on(TOPIC.PRIVATE_MESSAGE, privateMessageHandler);
    scrollToBottom();

    return () => {
      socket?.off(TOPIC.PRIVATE_MESSAGE);
      socket?.off(TOPIC.BROADCAST);
    };
  }, [socket,scrollToBottom,privateMessageHandler]);

  return {
    bottomRef,
    scrollToBottom,
    selectChat,
    allConversation,
    conversationID,
    setAllConversation,
  };
};

export const useMessageInputController = () => {
  const [text, setText] = useState<string>("");
  const setTextStates = useCallback((words: string) => {
    setText(words);
  }, []);
  const { errorRepository, authRepository, messageRepository, conferenceRepository } = useContext(RepositoryContext)!;
  const { setError } = errorRepository.errorRepo;
  const { auth } = authRepository.authStates;
  const { allConversation, setAllConversation } = messageRepository.chatListStates;
  const { conversationID } = messageRepository.selectedChatStates;
  const { setUsers } = messageRepository.userStates;
  const {setShowOutgoing} = conferenceRepository.showOutgoingModelStates;
  const {setHostProfileID} = conferenceRepository.hostStates
  const {socket} = useContext(SocketContext);
  const {peerRef} = useContext(PeerContext);

  const profile = auth.profile;
  const token = String(auth.token?.accessToken);

  const sendMessage = useCallback(async () => {
    const payload = {
      conversationID: conversationID,
      content: text,
      senderProfileID: String(profile?.id),
      readBy: [String(profile?.id)],
    };
    if (text.length > 0) {
      const { res, err } = await superagentReq({
        type: HTTP.POST,
        url: ENUM_URL.MESSAGE.CREATE,
        token: token,
        payload,
      });
      if (err) {
        setError({ message: String(err) });
      }
      if (res) {
        const newAllConversation:IAllConversations = {    
          ...allConversation,
          ...{
            [conversationID]: {
              users: allConversation[conversationID].users || [],
              chats: [...allConversation[conversationID].chats,{
                sender:{
                  name:profile?.name as string,
                  profileID:profile?.id as string
                },
                text: payload.content,
                read:true
              }],
              lastChat: payload.content,
              type: allConversation[conversationID].type,
              name: allConversation[conversationID].name || "",
            },
          }
        }
        setAllConversation(newAllConversation)
        sessionStorage.setItem("allConversation",JSON.stringify(newAllConversation))
   
        setUsers( prev =>{
          const newUsers = prev.map((user)=> (user.conversationID == conversationID) ? { ...user, ...{ text: String(payload.content)  }} : user)
          sessionStorage.setItem("users",JSON.stringify(newUsers))
          return newUsers
        })
        setTextStates("");
      }
    }
  }, [text,conversationID, allConversation,setAllConversation,setTextStates,profile?.id,profile?.name,setError,token,setUsers]);

  const startVideoCall:MouseEventHandler<HTMLAnchorElement> = (e) => {
    e.preventDefault();
    const profileIDs = allConversation[conversationID].users.map(user=>user.profileID).filter(pid=>pid!=profile?.id)
    socket?.emit(TOPIC.CONFERENCE_INVITATION,profileIDs,conversationID,profile?.id,{date:new Date().toISOString()})
    socket?.emit(TOPIC.JOIN_CONFERENCE, conversationID, profile?.id, profile?.name)
    setHostProfileID(profile!.id)
    peerRef!.current = new Peer({
      initiator: true,
      trickle:false,
    });
    setShowOutgoing(true)
  }
  return { sendMessage, text, setTextStates, profile, startVideoCall };
};

export const useMessageSpaceController = ({ chats }: IChat) => {
  const { authRepository } = useContext(RepositoryContext)!;
  const { auth } = authRepository.authStates;
  const bottomRef = useRef<HTMLDivElement>(null);
  const scrollToBottom = useCallback(() => {
    bottomRef.current?.scrollIntoView({ behavior: "smooth" });
  }, []);

  const profileID = String(auth.profile?.id);
  // const name = String(auth.profile?.name)
  const conversation: Array<{
    sender: { profileID: string; name: string };
    text: string;
  }> = chats || [];
  useEffect(() => {
    scrollToBottom();
  }, [scrollToBottom]);

  return { bottomRef, auth, profileID, conversation };
};

export const useFriendListingController = () => {
  const { socket } = useContext(SocketContext);
  const { messageRepository } = useContext(RepositoryContext)!;
  const { allConversation, setAllConversation } = messageRepository.chatListStates;
  const {users, setUsers} = messageRepository.userStates;

  const { data: fetchedConversation } = useFetchConversationService();
  const { profile } = useSession();

  useEffect(() => {
    console.log("fetched", fetchedConversation);
   
    fetchedConversation?.forEach((conversation: IFetchedConversation) => {
      if (!allConversation[conversation._id]) {
        setAllConversation({
          ...allConversation,
          ...{
            [conversation._id]: {
              users: conversation.users,
              chats: [],
              lastChat: conversation?.lastMessage?.text,
              type: conversation.type,
              name: conversation.name,
            },
          },
        });
      }
      if(!users.find(user=>user.conversationID==conversation._id)){
        setUsers([...users,{
          conversationID: conversation._id,
          receiverProfileIDs: conversation.profileIDs,
          name: conversation.name,
          online: false,
          unreadMessageCount:conversation?.unreadMessagesCount? conversation?.unreadMessagesCount[profile.id] : 0,
          text: conversation?.lastMessage?.text||"",
        }])
        sessionStorage.setItem("users",JSON.stringify(users))
      }
    });

  }, [allConversation, setAllConversation, fetchedConversation, profile.id, setUsers,users]);

  const resetAllOnlineUserToOffline = useCallback(()=>{
      setUsers(prev=>prev.map((user)=>({...user, ...{online:false}})))
    }
  ,[setUsers])

  const setUserToOnline = useCallback(
    (onlineUser:{name: string, profileID: string}) => {
      const {name,profileID} = onlineUser;
      setUsers(
        (prev)=>{
          return prev.map(user=>{
            return (user.receiverProfileIDs.some((pid) => pid === profileID) && user.name === name)?{
              ...user,...{online:true}
            }:user
          })
        }
      )
      sessionStorage.setItem("users",JSON.stringify(users))
    },
    [users,setUsers]
  );

  useEffect(() => {
    socket?.on(
      TOPIC.ONLINE,
      (onlineUsers: Array<{ name: string; profileID: string }>) => {
          const uniqueUsers = uniqBy(onlineUsers, "profileID");
          resetAllOnlineUserToOffline()
          uniqueUsers.forEach(setUserToOnline)
         
      }
    );
  }, [socket, resetAllOnlineUserToOffline, setUserToOnline]);

  return { users, setUsers, setUserToOnline };
};
