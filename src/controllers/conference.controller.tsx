import { MouseEventHandler, useCallback, useContext, useEffect, useRef, useState } from "react";
import { RepositoryContext } from "../repositories/repositoryContext";
import { SocketContext } from "../helpers/websocket/socketProvider";
import { TOPIC } from "../helpers/constants/topics";
import SimplePeer,{SignalData} from "simple-peer"
import { PeerComponent } from "../models/conference";

export const useMediaDevice = ({audio,video}:{audio:boolean,video:boolean}) =>{
    const [mediaStream,setMediaStream] = useState<MediaStream>()
    const [mediaStreamErr,setMediaStreamErr] = useState<Error>()
    
    useEffect(()=>{
        void ( async()=>{
            try{
                const stream = await  navigator.mediaDevices.getUserMedia({audio,video})
                setMediaStream(stream)
            }catch(error){
                setMediaStreamErr(error as Error)
            }
        })()
     
    },[setMediaStream,setMediaStreamErr,audio,video])

    return {mediaStream,setMediaStream,mediaStreamErr}
}

export const useConferenceInvitationController = () => {
    const { socket } = useContext(SocketContext)
    const { conferenceRepository, authRepository } =  useContext(RepositoryContext)!
    const { setShowIncoming } = conferenceRepository.showIncomingModelStates
    const { incomingData,setIncomingData} = conferenceRepository.incomingDataStates
    const { setShowConference } = conferenceRepository.showConferenceStates
    const { setHostProfileID } = conferenceRepository.hostStates
    const {auth} = authRepository.authStates

    const onConferenceInvitation = useCallback(({conversationID,message,hostProfileID}:{conversationID:string,message:{[key:string]:any},hostProfileID:string})=>{
        setShowIncoming(true);
        setIncomingData({conversationID,message,hostProfileID});
    },[setShowIncoming,setIncomingData]);


    const onAcceptInvitation:MouseEventHandler<HTMLButtonElement> = (e)=>{
        e.preventDefault()
        socket?.emit(TOPIC.CONFERENCE_INVITATION_ACCEPTED,{
            hostProfileID:incomingData?.hostProfileID,
            profileID:auth.profile?.id,
            name:auth.profile?.name
        });
        socket?.emit(TOPIC.JOIN_CONFERENCE, incomingData?.conversationID, auth.profile?.id,auth.profile?.name)
        setHostProfileID( incomingData!.hostProfileID)
        setShowIncoming(false)
        setShowConference(true)
    }

    useEffect(()=>{
        socket?.on(TOPIC.CONFERENCE_INVITATION,onConferenceInvitation)
        return ()=>{
            socket?.off(TOPIC.CONFERENCE_INVITATION)
        }
    },[socket,onConferenceInvitation]);

    return {onAcceptInvitation}
}


export const useConferenceEventControllers = () => {
    const localVideoRef = useRef<HTMLVideoElement>(null);
    const remoteVideosRef = useRef<HTMLDivElement>(null);
    const { socket } = useContext(SocketContext)
    const {conferenceRepository,authRepository} = useContext(RepositoryContext)!
    const { peerUsersStates } = conferenceRepository;
    const { auth } = authRepository.authStates;
    const {  peerUsers,setPeerUsers } = peerUsersStates;

    const createPeer = useCallback((profileID: string, socketIDs: string[], offer?: SignalData) => {
        const peer = new SimplePeer({
          initiator: !offer,
          stream: localVideoRef.current?.srcObject as MediaStream,
        });
    
        if (offer) {
          peer.signal(offer);
        }
    
        peer.on('signal', (data) => {
          socket?.emit('offer', data, profileID);
        });
    
        peer.on('connect', () => {
          console.log(`Connected to user ${profileID}`);
        });
    
        peer.on('stream', (remoteStream) => {
          const remoteVideo = document.createElement('video');
          remoteVideo.srcObject = remoteStream;
          remoteVideo.setAttribute('autoplay', 'true');
          remoteVideosRef.current?.appendChild(remoteVideo);
        });
    
        return peer;
    },[socket]);

    const onPeerJoinConference = useCallback(({profileID,name,socketIDs}:{conversationID:string,profileID:string,name:string,socketIDs:string[]})=>{
        const peer = createPeer(profileID,socketIDs, undefined);
        setPeerUsers(prev=>{
            const found = prev.find(peerUser=>peerUser.profileID == profileID)
            if(found) return prev.map(peerUser=>{
                return (peerUser.profileID==profileID)? {...peerUser,...{peer:peer}}:peerUser
            })
            return [...prev,({profileID, name, peer:peer, video:true, audio:true, initiator:false} as PeerComponent)]
        })
    },[createPeer,setPeerUsers]);
    
    const onPeerOffering = useCallback(({ offer, socketID, profileID, name}:{ offer:SignalData, socketID:string, profileID:string, name:string})=>{
        const peer = createPeer(profileID,[socketID], offer);
        setPeerUsers(prev=>{
            const found = prev.find(peerUser=>peerUser.profileID == profileID)
            if(found) return prev.map(peerUser=>{
                return (peerUser.profileID==profileID)? {...peerUser,...{peer:peer}}:peerUser
            })
            return [...prev,({profileID, name, peer:peer, video:true, audio:true, initiator:false} as PeerComponent)]
        })
    },[createPeer,setPeerUsers]);

    const onPeerAnswering = useCallback(({answer,profileID}:{answer:SignalData, socketID:string, profileID:string})=>{
        setPeerUsers(prev=>{
            return prev.map(peerUser=>{
                if(peerUser.profileID!=profileID) return peerUser
                peerUser.peer?.signal(answer)
                return {...peerUser,...{profileID}}
            })
        })
    },[setPeerUsers]);

    const onIceCandidate = useCallback(({candidate,profileID}:{candidate:SignalData, socketID:string, profileID:string})=>{
        setPeerUsers(prev=>{
            return prev.map(peerUser=>{
                if(peerUser.profileID!=profileID) return peerUser
                peerUser.peer?.signal(candidate)
                return {...peerUser,...{profileID}}
            })
        })
    },[setPeerUsers]);

    const onPeerLeft = useCallback(({profileID}:{socketID:string,profileID:string})=>{
        setPeerUsers(prev=>{
            return prev.filter(peerUser=>{
                if(peerUser.profileID!=profileID) return true
                peerUser.peer?.destroy()
                return false
            })
        })
    },[setPeerUsers]);
    const self = peerUsers.find(user=>user.profileID==auth.profile?.id)
    const {mediaStream} = useMediaDevice({audio:self!.audio,video:self!.video})
    
    useEffect(()=>{
        localVideoRef.current!.srcObject = mediaStream as MediaStream;
        socket?.on(TOPIC.JOIN_CONFERENCE,onPeerJoinConference)
        socket?.on(TOPIC.CONFERENCE_PEER_OFFERING,onPeerOffering)
        socket?.on(TOPIC.CONFERENCE_PEER_ANSWERING,onPeerAnswering)
        socket?.on(TOPIC.CONFERENCE_ICE_CANDIDATE,onIceCandidate)
        socket?.on(TOPIC.PEER_LEFT_CONFERENCE,onPeerLeft)
        return ()=>{ 
            socket?.off(TOPIC.JOIN_CONFERENCE) 
            socket?.off(TOPIC.CONFERENCE_PEER_OFFERING)
            socket?.off(TOPIC.CONFERENCE_PEER_ANSWERING)
            socket?.off(TOPIC.CONFERENCE_ICE_CANDIDATE)
            socket?.off(TOPIC.PEER_LEFT_CONFERENCE)
        }
    },[socket,mediaStream,onPeerJoinConference,onPeerOffering,onPeerAnswering,onIceCandidate,onPeerLeft])





}