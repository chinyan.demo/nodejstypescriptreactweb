/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { HTTP } from "../../helpers/constants/https"
import ENUM_URL from "../../helpers/constants/url"
import { superagentReq } from "../../helpers/request"

export const refreshTokenService = async(profileID:string, refreshToken:string) => {
    const {res,err} = await superagentReq<{token:{accessToken:string, refreshToken:string}}>({
        type: HTTP.POST,
        url: ENUM_URL.AUTH.REFRESH_TOKEN,
        token: refreshToken,
        payload:{profileID}
      })
      return {res,err}
}