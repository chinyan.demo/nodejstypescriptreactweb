import { useContext } from "react";
import ENUM_URL from "../../helpers/constants/url";
import { useGet } from "../../helpers/hooks/xhr";
import { RepositoryContext } from "../repositoryContext";
import { IFetchedConversation } from "../../models/message";
import { superagentReq } from "../../helpers/request";
import { HTTP } from "../../helpers/constants/https";

/**
 * can api to get online user from socket
 */
export const useOnlineUserFetch = <T,>() => {
  const { authRepository } = useContext(RepositoryContext)!;
  const { auth } = authRepository.authStates;
  const token = auth.token?.accessToken;
  const profileID = String(auth.profile?.id);
  return useGet<T>(
    ENUM_URL.MESSAGE.ONLINE_USER_LIST + "?profileID=" + profileID,
    token
  );
};

export const useFetchConversationService = () => {
  const { authRepository } = useContext(RepositoryContext)!;
  const { auth } = authRepository.authStates;
  const token = auth.token?.accessToken;
  const profileID = String(auth.profile?.id);
  return useGet<Array<IFetchedConversation>>(
    ENUM_URL.CONVERSATION.LIST + "?profileID=" + profileID,
    token
  );
};

export const useFetchMessagesService = (
  conversationID: string,
  pagination = { limit: 0, offset: 100 }
) => {
  const { authRepository } = useContext(RepositoryContext)!;
  const { auth } = authRepository.authStates;
  const token = auth.token?.accessToken;
  const payload = {
    conversationID,
    pagination,
  };
  return useGet<Array<IFetchedConversation>>(
    ENUM_URL.MESSAGE.LIST,
    token,
    payload
  );
};

export const updateMessageReadByService = async(payload:{conversationID:string,profileID:string},token:string) => {
  const {res,err} = await superagentReq({
    type: HTTP.PUT,
    url: ENUM_URL.MESSAGE.READ,
    token,
    payload
  })
  return {res,err}
}
