/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { createContext, JSX, FC } from 'react';
import {  useVideoCallRepo } from './states/videocall.states';
import { IVideoCallRepository } from '../models/videocall';
import { useErrorRepo } from './states/error.states';
import { IErrorRepo } from '../models/error';
import { IAuthRepo } from '../models/auth';
import { useAuthRepo } from './states/auth.states';
import { IMessagesRepository } from '../models/message';
import { useMessageRepo } from './states/messages.states';
import { IConferenceRepository } from '../models/conference';
import { useConferenceRepo } from './states/conference.states';

type RepositoryType = {
    videoCallRepository:IVideoCallRepository,
    errorRepository: IErrorRepo,
    authRepository: IAuthRepo,
    messageRepository: IMessagesRepository,
    conferenceRepository: IConferenceRepository

} | undefined

export const RepositoryContext = createContext<RepositoryType>(undefined);

export const RepositoryProvider:FC<{children:JSX.Element}> = ({children}) => {

    return (
        <RepositoryContext.Provider value={{
            videoCallRepository: useVideoCallRepo(),
            errorRepository: useErrorRepo(),
            authRepository: useAuthRepo(),
            messageRepository: useMessageRepo(),
            conferenceRepository: useConferenceRepo()
        }}>
            {children}
        </RepositoryContext.Provider>
    )
}