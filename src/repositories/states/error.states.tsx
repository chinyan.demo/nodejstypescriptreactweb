import { useState } from "react"
import { IError, IErrorRepo } from "../../models/error"

export const initErrorStates = {
    code: 200,
    message: "Ok",
    info: "Ok"
}

export const useErrorRepo = ():IErrorRepo => {
    const [error, setError] = useState<IError>(initErrorStates)
    const [socketError, setSocketError] = useState<IError>(initErrorStates)

    return { 
        errorRepo: {error, setError},
        socketErrorRepo: {socketError, setSocketError}
    }
}