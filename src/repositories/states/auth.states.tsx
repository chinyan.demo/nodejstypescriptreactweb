import { useState } from "react";
import { IAuth, IAuthRepo } from "../../models/auth";
import { IProfile } from "../../models/profile";

export const useAuthRepo = (): IAuthRepo => {
  const initSession = sessionStorage.getItem("session")
    ? (JSON.parse(sessionStorage.getItem("session") as string) as {
        profile: IProfile;
        token: { accessToken: string; refreshToken: string };
      })
    : {};
  const [auth, setAuth] = useState<IAuth>(initSession);

  return {
    authStates: { auth, setAuth },
  };
};
