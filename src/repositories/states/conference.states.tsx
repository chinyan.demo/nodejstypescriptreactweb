import { useState } from "react";
import { IConferenceRepository, PeerComponent, IncomingData } from "../../models/conference";
import { IAuth } from "../../models/auth";

export const useConferenceRepo = ():IConferenceRepository => {
    const selfProfile =  (JSON.parse( sessionStorage.getItem("auth") as string) as IAuth)?.profile 
    const [peerUsers,setPeerUsers] = useState<Array<PeerComponent>>([{   
        profileID: String(selfProfile?.id),
        name: String(selfProfile?.name),
        video:true,
        audio:true,
        initiator:false
    }])
    const [focusProfileID,setFocusProfileID] = useState<string>("")
    const [hostProfileID,setHostProfileID] = useState<string>("")
    const [ended, setEnded] = useState<boolean>(true);
    const [showConference, setShowConference] = useState<boolean>(false);
    const [showIncoming,setShowIncoming] = useState<boolean>(false);
    const [showOutgoing,setShowOutgoing] = useState<boolean>(false);
    const [incomingData,setIncomingData] = useState<IncomingData|undefined>()

    return {
        peerUsersStates: {peerUsers, setPeerUsers},
        focusedStates: {focusProfileID,setFocusProfileID},
        hostStates: {hostProfileID,setHostProfileID},
        conferenceEndedStates: {ended,setEnded},
        showConferenceStates: {showConference, setShowConference},
        showIncomingModelStates: {showIncoming,setShowIncoming},
        showOutgoingModelStates: {showOutgoing, setShowOutgoing},
        incomingDataStates: {incomingData,setIncomingData}
    }
}