import { useState } from "react";
import { IAllConversations, IMessagesRepository, IUser } from "../../models/message";

export const useMessageRepo = (): IMessagesRepository => {

  const initAllConversationState: IAllConversations = sessionStorage.getItem("allConversation")? JSON.parse(String(sessionStorage.getItem("allConversation"))) as IAllConversations : {};
  const initConversationID:string = sessionStorage.getItem("conversationID") || "bot";
  const initUser:Array<IUser> = JSON.parse(String(sessionStorage.getItem("users"))) as Array<IUser> || [];

  const [allConversation, setAllConversation] = useState<IAllConversations>(initAllConversationState);
  const [conversationID, setConversationID] = useState<string>(initConversationID);
  const [users, setUsers] = useState<Array<IUser>>(initUser);

  return {
    chatListStates: { allConversation, setAllConversation },
    selectedChatStates: { conversationID, setConversationID },
    userStates: {users, setUsers}
  };
};
