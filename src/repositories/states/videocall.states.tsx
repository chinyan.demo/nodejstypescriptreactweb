import { useState } from "react"
import { CallingComponent, IVideoCallRepository } from "../../models/videocall";

export const useVideoCallRepo = ():IVideoCallRepository => {
    const [incoming, setIncoming] = useState<CallingComponent>()
    const [outGoing, setOutGoing] = useState<CallingComponent>()
    const [show, setShow] = useState<boolean>(false);

    const [socketID, setSocketID] = useState<string>("")
    const [stream, setStream] = useState<MediaStream>()
    const [receivingCall, setReceivingCall] = useState<boolean>(false)
    const [caller, setCaller] = useState<{name:string,profileID:string}>({name:"",profileID:""})
    const [callerSignal, setCallerSignal] = useState<MediaStream>()
    const [callAccepted, setCallAccepted] = useState<boolean>(false)
    const [idToCall, setIdToCall] = useState<string>("")
    const [callEnded, setCallEnded] = useState<boolean>(false)
    const [name, setName] = useState<string>("")
    const [onlineUser, setOnlineUser] = useState<Array<{name:string,profileID:string}>>([])



    return { 
        inComingRepo: {incoming, setIncoming},
        outGoingRepo: {outGoing, setOutGoing},
        popUpRepo: {show, setShow},

        socketRepo: {socketID, setSocketID},
        streamRepo: {stream, setStream},
        receivingCallRepo : {receivingCall, setReceivingCall},
        callerRepo : {caller, setCaller},
        callerSignalRepo: {callerSignal, setCallerSignal},
        callAcceptedRepo: {callAccepted, setCallAccepted},
        idToCallRepo: {idToCall, setIdToCall},
        callEndedRepo: {callEnded, setCallEnded},
        nameRepo: {name, setName},
        onlineuserStates: {onlineUser, setOnlineUser}
    }
}



