import React from "react";
import "./App.css";
import MainRouter from "./routers";
import { RepositoryProvider } from "./repositories/repositoryContext";
import { WorkerProvider } from "./worker/workerContext";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import { ErrorHandler } from "./views/components/error";

const App: React.FC = () => {
  return (
    <>
        <RepositoryProvider>
          <ErrorHandler>
            <WorkerProvider>
              <MainRouter />
            </WorkerProvider>
          </ErrorHandler>
        </RepositoryProvider>
    </>
  );
};

export default App;
