const sampleTexts:Array<string> = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non tellus orci ac auctor augue mauris. Sagittis purus sit amet volutpat. Duis at consectetur lorem donec massa sapien faucibus. Penatibus et magnis dis parturient montes nascetur ridiculus mus mauris. Ut eu sem integer vitae justo eget. Sit amet nulla facilisi morbi. Et netus et malesuada fames ac turpis egestas. Ut porttitor leo a diam sollicitudin tempor id. Vitae justo eget magna fermentum iaculis eu non diam. Diam phasellus vestibulum lorem sed risus. Dui ut ornare lectus sit amet. Lacinia quis vel eros donec ac odio tempor. Mattis pellentesque id nibh tortor id aliquet. Vel pretium lectus quam id leo in vitae turpis.",
    "Venenatis lectus magna fringilla urna. Turpis egestas maecenas pharetra convallis posuere morbi. Libero id faucibus nisl tincidunt eget nullam non nisi est. Justo nec ultrices dui sapien eget mi. Felis donec et odio pellentesque. Sit amet nisl purus in mollis nunc. Amet nisl purus in mollis. A condimentum vitae sapien pellentesque habitant morbi tristique senectus et. Sagittis vitae et leo duis ut diam. Penatibus et magnis dis parturient montes nascetur ridiculus. Aenean sed adipiscing diam donec adipiscing tristique risus nec feugiat. Amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus et. Feugiat in ante metus dictum. Nec feugiat nisl pretium fusce id velit ut. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam. Pellentesque pulvinar pellentesque habitant morbi tristique. Quis viverra nibh cras pulvinar mattis nunc sed blandit libero. Eros donec ac odio tempor orci dapibus. Id faucibus nisl tincidunt eget nullam non nisi. Massa tincidunt dui ut ornare lectus sit amet.",
    "Diam quam nulla porttitor massa id neque. Vel turpis nunc eget lorem dolor sed. Sed enim ut sem viverra aliquet eget sit amet. Blandit aliquam etiam erat velit scelerisque in. Habitasse platea dictumst vestibulum rhoncus. Elementum eu facilisis sed odio. Eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Gravida dictum fusce ut placerat orci nulla pellentesque dignissim enim. Sit amet tellus cras adipiscing. Pellentesque elit eget gravida cum sociis natoque. Diam donec adipiscing tristique risus nec feugiat in fermentum posuere. Integer eget aliquet nibh praesent tristique magna sit. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam. Ultrices vitae auctor eu augue. Odio euismod lacinia at quis risus sed. Nunc faucibus a pellentesque sit amet porttitor. Tellus at urna condimentum mattis pellentesque id nibh tortor. Et netus et malesuada fames. Cursus mattis molestie a iaculis at erat pellentesque.",
    "Auctor urna nunc id cursus. In egestas erat imperdiet sed euismod nisi porta. Eu consequat ac felis donec et odio pellentesque. Eros in cursus turpis massa tincidunt. Viverra maecenas accumsan lacus vel facilisis. Blandit libero volutpat sed cras ornare. Eget mauris pharetra et ultrices. At in tellus integer feugiat scelerisque varius morbi enim. Velit euismod in pellentesque massa placerat duis ultricies lacus. At risus viverra adipiscing at in tellus. Purus sit amet luctus venenatis. Amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Lobortis scelerisque fermentum dui faucibus in. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Neque gravida in fermentum et sollicitudin ac orci phasellus egestas. Eget aliquet nibh praesent tristique magna sit amet purus gravida.",
    "Velit dignissim sodales ut eu sem integer vitae. Leo in vitae turpis massa sed elementum tempus. In pellentesque massa placerat duis ultricies. Amet luctus venenatis lectus magna. Egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Euismod in pellentesque massa placerat duis ultricies. Erat velit scelerisque in dictum non consectetur. Pellentesque nec nam aliquam sem et tortor consequat. Nunc aliquet bibendum enim facilisis gravida neque convallis a. Faucibus vitae aliquet nec ullamcorper sit amet risus. Tristique senectus et netus et malesuada fames ac turpis.",
];

export const LoremIpsumGenerator = (size:number) =>{
    const sentences = []
    for(let i = 0; i<size; i++){
        const sentenceID:number = Math.floor(Math.random()*sampleTexts.length)
        const id_1 = Math.floor(Math.random() * sampleTexts[sentenceID].length)
        let id_2 = Math.floor(Math.random() * sampleTexts[sentenceID].length)
        while(id_2==id_1){ id_2 = Math.floor(Math.random() * sampleTexts[sentenceID].length) }
        const startID = (id_1<id_2)? id_1 : id_2;
        const endID = (id_1<id_2)? id_2 : id_1;
        const sentence:string = sampleTexts[sentenceID].substring(startID,endID)
        sentences.push(sentence)
    }
    return sentences;
}

export const conversationGenerator = ({senders,size}:{ senders:Array<{profileID:string,name:string}>, size:number}) => {
    const conversations = [];
    for(let i=0; i<size; i++){
        const {profileID,name} = senders[Math.floor(Math.random()*senders.length)]
        conversations.push({sender:{name,profileID},text:LoremIpsumGenerator(1)[0]})
    }
    return conversations;
}