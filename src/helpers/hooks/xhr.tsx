import { useState, useEffect, useContext } from "react"
import { HTTP } from "../constants/https";
import { RepositoryContext } from "../../repositories/repositoryContext";

interface ResponseInterface<T> {
    data: T | null;
    loading: boolean;
    error: Error | string | null;
  }

export const useGet = <T,>(url:string,token="",payload?:any):ResponseInterface<T> => {
    const [data, setData] = useState<T | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const {errorRepository} = useContext(RepositoryContext)!
    const {error,setError} = errorRepository.errorRepo;

    useEffect(() => {
        setLoading(true);
        try{
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200){
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    const res:{success:boolean,errorObject?:any,data?:T} = JSON.parse(xhr.responseText);
                    if(res.success) setData(res?.data as T)
                    if(!res.success) setError({ code:xhr.status|500, message:JSON.stringify(res.errorObject) });
                }
            }
            xhr.open(HTTP.GET, url, true);
            if(token.length>0) xhr.setRequestHeader('Authorization',`Bearer ${token}`);
            if(!payload)xhr.send();
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            if(payload)xhr.send(payload);
        }catch(err){
            setError({code:500,message:JSON.stringify(err)})
        }
        setLoading(false);
    }, [url,token]);
  
    return { data, loading, error: error.message };
}

export const usePost = <T,>(url:string,payload:any,token=""):ResponseInterface<T> => {
    const [data, setData] = useState<T | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const {errorRepository} = useContext(RepositoryContext)!
    const {error,setError} = errorRepository.errorRepo;

    useEffect(() => {
        setLoading(true);
        try{
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200){
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    const res:{success:boolean,errorObject?:any,data?:T} = JSON.parse(xhr.responseText);
                    if(res.success)setData(res?.data as T)
                    if(!res.success) setError({code:xhr.status|500, message:res.errorObject as string });
                    
                }
            }
            xhr.open(HTTP.POST, url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            if(token.length>0) xhr.setRequestHeader('Authorization',`Bearer ${token}`);
            xhr.send(JSON.stringify(payload));
        }catch(err){
            setError({code:500, message:err as string})
        }

        setLoading(false);
    }, [url,payload,token]);
  
    return { data, loading, error: error.message };
}

export const usePut = <T,>(url:string,payload:any,token=""):ResponseInterface<T> => {
    const [data, setData] = useState<T | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const {errorRepository} = useContext(RepositoryContext)!
    const {error, setError} = errorRepository.errorRepo;

    useEffect(() => {
        setLoading(true);
        try{
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200){
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    const res:{success:boolean,errorObject?:any,data?:T} = JSON.parse(xhr.responseText);
                    if(res.success)setData(res?.data as T)
                    if(!res.success) { setError({ code: xhr.status|500, message: res.errorObject as string}) }
                }
            }
            xhr.open(HTTP.PUT, url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            if(token.length>0) xhr.setRequestHeader('Authorization',`Bearer ${token}`);
            xhr.send(JSON.stringify(payload));
        }catch(err){
            setError({ code:500, message:err as string})
        }
        setLoading(false);
    }, [url,payload,token]);
  
    return { data, loading, error: error.message };
}

export const useDelete = <T,>(url:string,payload:any,token=""):ResponseInterface<T> => {
    const [data, setData] = useState<T | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const {errorRepository} = useContext(RepositoryContext)!
    const {error, setError} = errorRepository.errorRepo;

    useEffect(() => {
        setLoading(true);
        try{
            const xhr = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200){
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    const res:{success:boolean,errorObject?:any,data?:T} = JSON.parse(xhr.responseText);
                    if(res.success)setData(res?.data as T)
                    if(!res.success) {
                        setError({code: xhr.status|500, message: res.errorObject as string})
                    }
                }
            }
            xhr.open(HTTP.DELETE, url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            if(token.length>0) xhr.setRequestHeader('Authorization',`Bearer ${token}`);
            xhr.send(JSON.stringify(payload));
        }catch(err){
            setError({ code:500, message:err as string });
        }
        setLoading(false);
    }, [url,payload,token]);
  
    return { data, loading, error: error.message };
}