import { IAuth } from "../../models/auth";
import { IProfile } from "../../models/profile";

const useSession = () => {
  const profileString = String(sessionStorage.getItem("session"));

  const profileParent = profileString ? JSON.parse(profileString) as IAuth : {};
  const profile: IProfile = profileParent?.profile as IProfile ;

  return { profile };
};

export default useSession;
