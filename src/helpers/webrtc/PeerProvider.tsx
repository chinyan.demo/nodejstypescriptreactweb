import { MutableRefObject, createContext, useRef, JSX, FC } from "react";
import  { Instance } from "simple-peer";

export const PeerContext = createContext<{peerRef:MutableRefObject<Instance|undefined>|undefined}>({peerRef:undefined})

export const PeerProvider:FC<{children:JSX.Element}> = ({children}) => {
    const peerRef = useRef<Instance|undefined>(undefined);

    // useEffect(()=>{
    //     const peer = peerRef.current

    //     peer?.on()
    // },[peerRef.current])
    return (
        <PeerContext.Provider value={{peerRef}}>
            {children}
        </PeerContext.Provider>
    )
} 