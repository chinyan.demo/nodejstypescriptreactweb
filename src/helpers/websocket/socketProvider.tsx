// import Peer from "simple-peer"
import { FC, useContext, useEffect, useState, JSX,createContext } from "react";
import {  getSocket } from "./socket";
import { TOPIC } from "../constants/topics";
import { RepositoryContext } from "../../repositories/repositoryContext";
import { initErrorStates } from "../../repositories/states/error.states";
import { Socket } from "socket.io-client";

export const SocketContext = createContext<{
  socket: Socket | undefined;
}>({ socket: undefined, });

export const SocketProvider: FC<{ children: JSX.Element }> = ({ children }) => {
  const { errorRepository, authRepository } = useContext(RepositoryContext)!;
  const { setSocketError } = errorRepository.socketErrorRepo;
  const { auth } = authRepository.authStates;
  const [socket, setSocket] = useState<Socket>();

  const profileID = auth?.profile?.id as string;
  const name = auth?.profile?.name as string;
  const accessToken = auth?.token?.accessToken as string;

  useEffect(()=>{
    const newSocket = (!socket || !socket?.connected)? getSocket(name, profileID, accessToken):socket
    setSocket(newSocket)

    return () => {
      newSocket.disconnect();
    };
  },[])

  const onConnectHandler = () => {
    if (socket?.connected) {
      setSocketError(initErrorStates);
      console.log("Connected to socket", socket.id);
   
    }
  };

  const onDisconnectedHandler = () => {
    if (!socket?.connected) {
      setSocketError({ code: 400, message: "Socket not connected" });
      console.log("disconnected from socket");
    }
  };

  const onErrorHandler = (err: { message: string }) => {
    if (!socket?.connected) {
      setSocketError({ code: 400, message: err.message });
      console.log("socket error : ", err);
      console.log(err.message);
    }
  };

  useEffect(() => {
    if (!auth?.token) {
      socket?.disconnect();
    }
    socket?.on(TOPIC.CONNECT_ERROR, onErrorHandler);
    socket?.on(TOPIC.CONNECT, onConnectHandler);
    socket?.on(TOPIC.CONNECTION_FAILED, onDisconnectedHandler);

    // return(()=>{socket.disconnect();})
  }, [socket, auth]);

  return (
    <SocketContext.Provider value={{ socket }}>
      {children}
    </SocketContext.Provider>
  );
};
