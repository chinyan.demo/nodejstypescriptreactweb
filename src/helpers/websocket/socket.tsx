/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { io as Client, Socket } from "socket.io-client";

let socket: Socket;

const WS_URL: string = String(import.meta.env.VITE_WEBSOCKET_URL) || "";
export const getSocket = (name: string, profileID: string, token: string):Socket => {
  console.log(WS_URL);
  socket = Client(WS_URL, {
    autoConnect: false,
    reconnection:false,
    transports: ["websocket"],
    path: "/websocket",
    auth: { token, profileID, name },
    // retries: 1,
  });
  if (!socket || !socket.connected) socket.connect();
  return socket;
};

// export const getPeer = (name: string, profileID: string): Peer => {
//   return new Peer(JSON.stringify({ profileID, name }), {
//     host: String(import.meta.env.VITE_PEER_HOST),
//     // port: parseInt(import.meta.env.VITE_PEER_PORT),
//     path: String(import.meta.env.VITE_PEER_URI),
//   });
// };


