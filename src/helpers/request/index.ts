/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import {HttpMethodType} from "../../models/form"
import {UrlString} from "../../models/form"
import superagent from "superagent"
import { HTTP } from "../constants/https"

export const request = <T>(input:{
    type:HttpMethodType,
    url:UrlString,
    token?:string,
    payload?:any,
}):Promise<{res?:T,err?:{detail:any,code?:number}}> => {
    return new Promise((resolve)=>{
        try{
            const {type,url,token,payload} = input
            const REQUEST_TYPE:HttpMethodType = type
            const URL = url
    
            const xhr = new XMLHttpRequest();
            xhr.open(REQUEST_TYPE, URL, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            if(token) xhr.setRequestHeader('Authorization',`Bearer ${token}`);
            if(payload){
                xhr.send(JSON.stringify(payload));
            }else{
                xhr.send();
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4 && xhr.status === 200){
                    console.log("ok, response :", xhr.responseText);
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    const response:T = JSON.parse(xhr.responseText);
                    resolve({res:response,err:undefined});
                
                }
                if(xhr.status >= 400){
                    const response =  JSON.parse(xhr.responseText) as {success:boolean, errorObject:any}
                    const detail = (xhr.responseText)? response.errorObject : xhr.statusText
                    resolve({ err:{detail:detail,code:xhr.status}, res:undefined})
                }
            }
            xhr.onerror = () => {
                resolve({ err:{detail:xhr.statusText,code:xhr.status}, res:undefined})
            };
          
        
        }catch(error:any){
            console.table({error})
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            if(error?.data)resolve({err:{detail:error?.data,code:error?.code},res:undefined})
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            if(!error?.data) resolve({err:{detail:error,code:500},res:undefined})
        }
    })
}

interface response<T> {success:boolean, errorObject?:any, data?: T, info?:any}

export const superagentReq = async<T>(input:{
        type:HttpMethodType,
        url:UrlString,
        token?:string,
        payload?:any,
    }) => {
        try{
            const {type,url,token,payload} = input
            const header = (token)?  {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            } : {"Content-Type": "application/json"}
            if(type==HTTP.GET){
                const res = await superagent.get(url).set(header).send(payload)
                if(res.statusCode>=300) throw res.text
                const response:response<T> = JSON.parse(res.text)
                if(!response.success) throw response
                return {res:response.data,err:undefined}
            }else if(type == HTTP.POST){
                const res = await superagent.post(url).set(header).send(payload)
                if(res.statusCode>=300) throw res.text
                const response:response<T>  = JSON.parse(res.text)
                if(!response.success) throw response
                return {res:response.data,err:undefined}
            }else if(type == HTTP.PUT){
                const res = await superagent.put(url).set(header).send(payload)
                if(res.statusCode>=300) throw res.text
                const response:response<T>  = JSON.parse(res.text)
                if(!response.success) throw response
                return {res:response.data,err:undefined}
            }else if(type == HTTP.DELETE){
                const res = await superagent.delete(url).set(header).send(payload)
                if(res.statusCode>=300) throw res.text
                const response:response<T>  = JSON.parse(res.text)
                if(!response.success) throw response
                return {res:response.data,err:undefined}
            }else{
                throw "Invalid HTTP method"
            }


        }catch(error:any){
            return {res:undefined,err:error}
        }
}

