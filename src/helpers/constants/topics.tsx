export enum TOPIC  {
    CONNECT = "connect",
    ONLINE = "online",
    INFO = "info",
    PRIVATE_MESSAGE = "private message",
    DISCONNECT = "disconnect",
    CONNECTION_FAILED = "connect_failed",
    ERROR = "error",
    AUTH_ERROR = "auth error",
    CONNECT_ERROR = "connect_error",
    
    JOIN_CONFERENCE = "user-joined-conference",
    PEER_LEFT_CONFERENCE = "user-left-conference",
    CONFERENCE_INVITATION = "conference-invitation",
    CONFERENCE_INVITATION_ACCEPTED = "conference-invitation-accepted",
    CONFERENCE_PEER_OFFERING = "conference-peer-offering",
    CONFERENCE_PEER_ANSWERING = "conference-peer-answering",
    CONFERENCE_ICE_CANDIDATE = "conference-ice-candidates",

    USER_CONNECTED = "user-connected",
    USER_DISCONNECT = "user-disconnected",
    VIDEO_CALL="video call",
    JOIN_VIDEO_CALL="join video call",
    BROADCAST = "broadcast",
    
    SOCKET_ID = "socketID",
    CALL_USER = "callUser",
    ANSWER_CALL = "answerCall",
    CALL_ACCEPTED = "callAccepted"
}

export enum PEER_TOPIC {
    CONNECTION = "connection",
    OPEN = "open",
    STREAM = "stream",
    CALL = "call",
    CLOSE = "close",

    SIGNAL = "signal",

    CONFERENCE_SIGNAL = "conference-signal",
    CONFERENCE_STREAM = "conference-stream"
}