import { UrlString } from "../../models/form";

const BASE_URL = String(import.meta.env.VITE_API_URL);

const ENUM_URL = Object.freeze({
  HEALTH_CHECK: `${BASE_URL}` as UrlString,
  AUTH: {
    REGISTER: `${BASE_URL}/v1.0/auth/register` as UrlString,
    LOGIN: `${BASE_URL}/v1.0/auth/login` as UrlString,
    REFRESH_TOKEN: `${BASE_URL}/v1.0/auth/refresh/token` as UrlString,
  },
  FRIEND: {
    LIST: `${BASE_URL}/v1.0/friend/list` as UrlString,
  },
  CONVERSATION: {
    LIST: `${BASE_URL}/v1.0/conversation/list` as UrlString,
  },
  MESSAGE: {
    CREATE: `${BASE_URL}/v1.0/message/create` as UrlString,
    ONLINE_USER_LIST: `${BASE_URL}/v1.0/message/online/user/list` as UrlString,
    LIST: `${BASE_URL}/v1.0/message/by/conversationID/list` as UrlString,
    READ: `${BASE_URL}/v1.0/message/readby/update` as UrlString
  }
});

export default ENUM_URL;
