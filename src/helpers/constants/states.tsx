enum STATES  {
    PROFILES = "PROFILES",
    ERROR = "ERROR",
    SOCKET_ERROR = "SOCKET_ERROR",
    AUTH = "AUTHENTICATION"
}

enum BASE_FIELD {
    id = "id"
}

export {
    STATES,
    BASE_FIELD
}