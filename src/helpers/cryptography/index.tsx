import CryptoJS,{SHA256} from 'crypto-js';


export const createHash = (str:string):string => {
    const hash = String(SHA256(str).toString(CryptoJS.enc.Hex))
    return hash
}
