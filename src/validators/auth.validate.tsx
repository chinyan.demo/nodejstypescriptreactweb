import joi from "joi"

export const nameValidate = joi.string().required()
export const emailValidate = joi.string().email({tlds:{allow: false}}).required()
export const passwordValidate = joi.string().required()

export const registerValidate = joi.object().keys({
    name:nameValidate,
    email:emailValidate,
    password: passwordValidate,
    confirmPassword: passwordValidate.valid(joi.ref("password")).messages({'any.only':'confirmPassword must match with password'})
});

export const loginValidate = joi.object().keys({
    email:joi.string().email({tlds:{allow: false}}).required(),
    password: joi.string().required(),
});